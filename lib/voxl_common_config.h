/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef VOXL_COMMON_CONFIG_H
#define VOXL_COMMON_CONFIG_H

#include <stdio.h>
#include <string.h>
#include <modal_json.h>


#ifdef __cplusplus
extern "C" {
#endif

/**
 * This is the default and recommended config file path and is used by both
 * voxl-vision-px4 and voxl-apriltag-server. However other projects can elect
 * to have their own independant config files if they wish.
 */
#define VCC_APRILTAG_CONFIG_PATH	"/etc/modalai/apriltags.conf"

/*
 * recommended sane but arbitrary limit on number of tags in one config file to
 * allow for static memory allocation. You can ignore this and set your own
 * limit if you want, both functions here allow setting max through arguments.
 */
#define VCC_MAX_APRILTAGS_IN_CONFIG 64


/**
 * An apriltag can be flagged by the user as fixed, static, or dynamic.
 *
 * "fixed": The tag is at a known location in space as described by the
 * T_tag_wrt_fixed vector and R_tag_to_fixed rotation matrix. These fixed tags
 * are used by voxl-vision-px4 for apriltag relocalization.
 *
 * "static": A static tag can be trusted to be static (not moving) but does not
 * have a known location. For example, a landing pad.
 *
 * "dynamic": A dynamic tag can be expected to be in motion such as an object to
 * be tracked or followed dynamically.
 */
#define VCC_LOCATION_T_STRINGS {"fixed","static","dynamic"}
typedef enum vcc_tag_location_t{
	LOCATION_FIXED		= 0,
	LOCATION_STATIC		= 1,
	LOCATION_DYNAMIC	= 2
} vcc_tag_location_t;


// struct to contain all data from each single tag entry in config file
typedef struct vcc_apriltag_t{
	int tag_id;						///< ID of the tag
	char name[64];					///< apriltag name, e.g. "landing pad"
	vcc_tag_location_t location;	///< location type: fixed, static, or dynamic
	double size_m;					///< tag size in meters, length of one edge
	double T_tag_wrt_fixed[3];		///< translation vector
	double R_tag_to_fixed[3][3];	///< rotation matrix
} vcc_apriltag_t;



/**
 * @brief      print out an array of apriltag configuration structs
 *
 * @param      t     pointer to conf struct array
 * @param[in]  n     number of tags to print
 */
void vcc_print_apriltag_conf(vcc_apriltag_t* t, int n);


/**
 * @brief      Reads an apriltag configuration file.
 *
 * @param[in]  path   file path, usually APRILTAG_CONFIG_PATH
 * @param      t      pointer to array of config structs to write out to
 * @param      n      pointer to write out number of tags parsed from the file
 * @param[in]  max_t  The maximum number of tags that array t can hold
 *
 * @return     0 on success, -1 on failure
 */
int vcc_read_apriltag_conf_file(const char* path, vcc_apriltag_t* t, int* n, int max_t);



////////////////////////////////////////////////////////////////////////////////

/**
 * This is the default and recommended config file path and is used by both
 * voxl-qvio-server, voxl-vins-server, voxl-dfs-server, and voxl-vision-px4.
 * However other projects can elect to have their own independant config files
 * if they wish.
 */
#define VCC_EXTRINSICS_PATH	"/etc/modalai/extrinsics.conf"

/*
 * Recommended sane but arbitrary limit on number of extrinsics in one config
 * file to allow for static memory allocation. You can ignore this and set your
 * own limit if you want, both functions here allow setting max through
 * arguments.
 */
#define VCC_MAX_EXTRINSICS_IN_CONFIG 32


/**
 * This configuration file serves to describe the static relations (translation
 * and rotation) between sensors and bodies on a drone. Mostly importantly it
 * configures the camera-IMU extrinsic relation for use by VIO. However, the
 * user may expand this file to store many more relations if they wish. By
 * consolidating these relations in one file, multiple processes that need this
 * data can all be configured by this one configuration file. Also, copies of
 * this file may be saved which describe particular drone platforms. The
 * defaults describe the VOXL M500 drone reference platform.
 *
 * The file is constructed as an array of multiple extrinsic entries, each
 * describing the relation from one parent to one child. Nothing stops you from
 * having duplicates but this is not advised.
 *
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in
 * intrinsic XYZ order in units of degrees. This corresponds to the parent
 * rolling about its X axis, followed by pitching about its new Y axis, and
 * finally yawing around its new Z axis to end up aligned with the child
 * coordinate frame.
 *
 * The helper read function will read out and populate the associated data
 * struct in both Tait-Bryan and rotation matrix format so the calling process
 * can use either. Helper functions are provided to convert back and forth
 * between the two rotation formats.
 *
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for
 * ease of use when doing camera-IMU extrinsic relations in the field. This is
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by
 * the rc_math library. However, since the camera Z axis points out the lens, it
 * is helpful for the last step in the rotation sequence to rotate the camera
 * about its lens after first rotating the IMU's coordinate frame to point in
 * the right direction by Roll and Pitch.
 *
 * The following online rotation calculator is useful for experimenting with
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/
 *
 * The Translation vector should represent the center of the child coordinate
 * frame with respect to the parent coordinate frame in units of meters.
 *
 * The parent and child name strings should not be longer than 63 characters.
 */
typedef struct vcc_extrinsic_t{
	char parent[64];				///< parent frame name, e.g. "imu0"
	char child[64];					///< child name, e.g. "tracking0" (camera)
	double T_child_wrt_parent[3];	///< translation vector, eg location of camera wrt imu frame
	double RPY_parent_to_child[3];	///< intrinsic Tait-Bryan angles in XYZ (Roll Pitch Yaw) sequence in degrees
	double R_child_to_parent[3][3];	///< rotation matrix, eg from imu to camera frame
} vcc_extrinsic_t;


/**
 * @brief      print out an array of extrinsic configuration structs
 *
 * @param      t       pointer to extrinsic struct array
 * @param[in]  n_tags  number of extrinsic config structs to print
 */
void vcc_print_extrinsic_conf(vcc_extrinsic_t* t, int n);


/**
 * @brief      Reads an extrinsic configuration file.
 *
 * @param[in]  path   file path, usually EXTRINSICS_PATH
 * @param      t      pointer to array of config structs to write out to
 * @param      n      pointer to write out number of tags parsed from the file
 * @param[in]  max_t  The maximum number of structs that array t can hold
 *
 * @return     0 on success, -1 on failure
 */
int vcc_read_extrinsic_conf_file(const char* path, vcc_extrinsic_t* t, int* n, int max_t);


/**
 * @brief      Finds an extrinsic configuration with specified parent and child
 *             in an array of extrinsics.
 *
 *             This is used to grab the extrinsic configuration that you want
 *             after loading everything from a file. If multiple extrinsic
 *             configurations exist with the desired parent and child then the
 *             first one will be returned.
 *
 * @param[in]  parent  The parent frame, e.g. "imu0"
 * @param[in]  child   The child frame, e.g. "tracking0" camera
 * @param      t       array of extrinsic_t structs to search through
 * @param[in]  n       number of structs in array t
 * @param      out     pointer to user's extrinsic_t struct to write out to
 *
 * @return     0 on success, -1 on failure
 */
int vcc_find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out);


/**
 * @brief      Convert a rotation matric to a Tait-Bryan intrinsic XYZ (Roll
 *             Pitch Yaw) rotation sequence in degrees
 *
 *             Unlike the rc_math library which consistently uses the more
 *             common aerospace-standard ZYX Tait-Bryan rotation sequence in
 *             radians, this function uses the XYZ Tait-Bryan rotation sequence
 *             in degrees. The reason for this is that a primary use for the
 *             extrinsics config file is setting the IMU-to-Camera extrinsic
 *             relation which is most easily done in the field by observation in
 *             units of degrees instead of radians. Also, since the camera Z
 *             axis points out the lens, it is helpful for the last step in the
 *             rotation sequence to rotate the camera about its lens after
 *             getting rotating it to point in the right direction by Roll and
 *             Pitch.
 *
 *             Note that a rotation sequence that intrinsically rotates a
 *             coordinate frame A to align with coordinate frame B will convert
 *             to a rotation matrix that rotates a vector from being represented
 *             in frame B to frame A.
 *
 * @param[in]  R     Rotation Matrix
 * @param[out] tb    resulting Tait-Bryan intrinsic XYZ (Roll Pitch Yaw)
 *                   rotation sequence in degrees
 */
void vcc_rotation_matrix_to_tait_bryan_xyz_degrees(double R[3][3], double tb[3]);


/**
 * @brief      convert Tait-Bryan intrinsic XYZ (Roll Pitch Yaw) rotation
 *             sequence in degrees to a rotation matrix
 *
 *             Unlike the rc_math library which consistently uses the more
 *             common aerospace-standard ZYX Tait-Bryan rotation sequence in
 *             radians, this function uses the XYZ Tait-Bryan rotation sequence
 *             in degrees. The reason for this is that a primary use for the
 *             extrinsics config file is setting the IMU-to-Camera extrinsic
 *             relation which is most easily done in the field by observation in
 *             units of degrees instead of radians. Also, since the camera Z
 *             axis points out the lens, it is helpful for the last step in the
 *             rotation sequence to rotate the camera about its lens after
 *             getting rotating it to point in the right direction by Roll and
 *             Pitch.
 *
 *             Note that a rotation sequence that intrinsically rotates a
 *             coordinate frame A to align with coordinate frame B will convert
 *             to a rotation matrix that rotates a vector from being represented
 *             in frame B to frame A.
 *
 * @param[in]  tb    tait-bryan intrinsic XYZ (Roll Pitch Yaw) rotation sequence
 * @param[out] R     Resulting rotation matrix
 */
void vcc_tait_bryan_xyz_degrees_to_rotation_matrix(double tb[3], double R[3][3]);


#ifdef __cplusplus
}
#endif

#endif // end #define VOXL_COMMON_CONFIG_H