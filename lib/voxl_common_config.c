/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <math.h>

#include <modal_json.h>
#include "voxl_common_config.h"



#define APRILTAG_CONFIG_FILE_HEADER "\
/**\n\
 * Apriltag Configuration File\n\
 * This file is used by both voxl-vision-px4 and voxl-apriltag-server.\n\
 *\n\
 * An apriltag can be flagged by the user as fixed, static, or dynamic.\n\
 *\n\
 *  - fixed: The tag is at a known location in space as described by the\n\
 *    T_tag_wrt_fixed vector and R_tag_to_fixed rotation matrix. These fixed\n\
 *    tags are used by voxl-vision-px4 for apriltag relocalization.\n\
 *\n\
 *  - static: A static tag can be trusted to be static (not moving) but does not\n\
 *    have a known location. For example, a landing pad.\n\
 *\n\
 *  - dynamic: A dynamic tag can be expected to be in motion such as an object\n\
 *    to be tracked or followed dynamically.\n\
 *\n\
 *\n\
 * If the tag is fixed then you must specify its location and rotation relative\n\
 * to the fixed reference frame. Otherwise the translation and rotation\n\
 * parameters can be ignored.\n\
 *\n\
 * The name parameter is currently only used for info prints but can be helpful\n\
 * for the user to keep track of what tag is which.\n\
 *\n\
 * Currently only 36h11 apriltags are supported\n\
 */\n"



void vcc_print_apriltag_conf(vcc_apriltag_t* t, int n)
{
	int i,j;
	const char* loc_strings[] = VCC_LOCATION_T_STRINGS;

	for(i=0; i<n; i++){
		printf("#%d:\n",i);
		printf("    tag id:          %d\n", t[i].tag_id);
		printf("    name:            %s\n", t[i].name);
		printf("    location:        %s\n", loc_strings[t[i].location]);
		printf("    size_m:          %7.3f\n", t[i].size_m);
		printf("    T_tag_wrt_fixed:");
		for(j=0;j<3;j++) printf("%4.1f ",t[i].T_tag_wrt_fixed[j]);
		printf("\n    R_tag_to_fixed: ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[0][j]);
		printf("\n                    ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[1][j]);
		printf("\n                    ");
		for(j=0;j<3;j++) printf("%4.1f ", t[i].R_tag_to_fixed[2][j]);
		printf("\n");
	}
	return;
}


int vcc_read_apriltag_conf_file(const char* path, vcc_apriltag_t* t, int* n, int max_t)
{
	// vars and defaults
	int i, m;
	double default_T[3]    = { 0,  0,  0};
	double default_R[3][3] ={{ 0, -1,  0}, \
							 { 1,  0,  0}, \
							 { 0,  0,  1}};
	const char* loc_strings[] = VCC_LOCATION_T_STRINGS;
	const int n_loc_strings = sizeof(loc_strings)/sizeof(loc_strings[0]);


	//sanity checks
	if(t==NULL || n==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}
	if(max_t<2){
		fprintf(stderr, "ERROR in %s, maximum number of apriltags must be >=2\n", __FUNCTION__);
		return -1;
	}

	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(path, APRILTAG_CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new empty json file: %s\n", path);

	// read the data in
	cJSON* parent = json_read_file(path);
	if(parent==NULL) return -1;

	// file structure is just one big array of apriltag config structures
	cJSON* apriltag_json = json_fetch_array_and_add_if_missing(parent, "apriltags", &m);
	if(m > max_t){
		fprintf(stderr, "ERROR found %d apriltags in file but maximum number of tags is set to %d\n", m, max_t);
		return -1;
	}

	// Empty file found, so add two new empty objects to the array.
	// Defaults will be added by json_fetch_xxx helpers next
	if(m == 0){
		cJSON_AddItemToArray(apriltag_json, cJSON_CreateObject());
		cJSON_AddItemToArray(apriltag_json, cJSON_CreateObject());
		m = 2;
		json_set_modified_flag(1); // log that we modified the parent
	}

	// copy out each item in the array
	for(i=0; i<m; i++){
		cJSON* item = cJSON_GetArrayItem(apriltag_json, i);
		json_fetch_int_with_default(		item, "tag_id",			&t[i].tag_id, i);
		json_fetch_string_with_default(		item, "name",			t[i].name, 64, "default_name");
		json_fetch_enum_with_default(		item, "location",		(int*)&t[i].location, loc_strings,	n_loc_strings,	0);
		json_fetch_double_with_default(		item, "size_m",			&t[i].size_m, 0.4);
		json_fetch_fixed_vector_with_default(item,"T_tag_wrt_fixed",t[i].T_tag_wrt_fixed, 3, default_T);
		json_fetch_fixed_matrix_with_default(item,"R_tag_to_fixed",	&t[i].R_tag_to_fixed[0][0], 3, 3, &default_R[0][0]);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", path);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON apriltag data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(path, parent, APRILTAG_CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	*n = m;
	return 0;
}


////////////////////////////////////////////////////////////////////////////////

#define EXTRINSIC_CONFIG_FILE_HEADER "\
/**\n\
 * Extrinsic Configuration File\n\
 * This file is used by voxl-vision-px4, voxl-qvio-server, and voxl-vins-server.\n\
 *\n\
 * This configuration file serves to describe the static relations (translation\n\
 * and rotation) between sensors and bodies on a drone. Mostly importantly it\n\
 * configures the camera-IMU extrinsic relation for use by VIO. However, the\n\
 * user may expand this file to store many more relations if they wish. By\n\
 * consolidating these relations in one file, multiple processes that need this\n\
 * data can all be configured by this one configuration file. Also, copies of\n\
 * this file may be saved which describe particular drone platforms. The\n\
 * defaults describe the VOXL M500 drone reference platform.\n\
 *\n\
 * The file is constructed as an array of multiple extrinsic entries, each\n\
 * describing the relation from one parent to one child. Nothing stops you from\n\
 * having duplicates but this is not advised.\n\
 *\n\
 * The rotation is stored in the file as a Tait-Bryan rotation sequence in\n\
 * intrinsic XYZ order in units of degrees. This corresponds to the parent\n\
 * rolling about its X axis, followed by pitching about its new Y axis, and\n\
 * finally yawing around its new Z axis to end up aligned with the child\n\
 * coordinate frame.\n\
 *\n\
 * The helper read function will read out and populate the associated data\n\
 * struct in both Tait-Bryan and rotation matrix format so the calling process\n\
 * can use either. Helper functions are provided to convert back and forth\n\
 * between the two rotation formats.\n\
 *\n\
 * Note that we elect to use the intrinsic XYZ rotation in units of degrees for\n\
 * ease of use when doing camera-IMU extrinsic relations in the field. This is\n\
 * not the same order as the aerospace yaw-pitch-roll (ZYX) sequence as used by\n\
 * the rc_math library. However, since the camera Z axis points out the lens, it\n\
 * is helpful for the last step in the rotation sequence to rotate the camera\n\
 * about its lens after first rotating the IMU's coordinate frame to point in\n\
 * the right direction by Roll and Pitch.\n\
 *\n\
 * The following online rotation calculator is useful for experimenting with\n\
 * rotation sequences: https://www.andre-gaschler.com/rotationconverter/\n\
 *\n\
 * The Translation vector should represent the center of the child coordinate\n\
 * frame with respect to the parent coordinate frame in units of meters.\n\
 *\n\
 * The parent and child name strings should not be longer than 63 characters.\n\
 *\n\
 * The relation from Body to Ground is a special case where only the Z value is\n\
 * read by voxl-vision-px4 and voxl-qvio-server so that these services know the\n\
 * height of the drone's center of mass (and tracking camera) above the ground\n\
 * when the drone is sitting on its landing gear ready for takeoff.\n\
 *\n\
 **/\n"




void vcc_print_extrinsic_conf(vcc_extrinsic_t* t, int n)
{
	int i,j;

	for(i=0; i<n; i++){
		printf("#%d:\n",i);
		printf("    parent:                %s\n", t[i].parent);
		printf("    child:                 %s\n", t[i].child);
		printf("    T_child_wrt_parent:  ");
		for(j=0;j<3;j++) printf("%7.3f ",t[i].T_child_wrt_parent[j]);
		printf("\n    RPY_parent_to_child:");
		for(j=0;j<3;j++) printf("%6.1f  ",t[i].RPY_parent_to_child[j]);
		printf("\n    R_child_to_parent:   ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[0][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[1][j]);
		printf("\n                         ");
		for(j=0;j<3;j++) printf("%7.3f ", t[i].R_child_to_parent[2][j]);
		printf("\n");
	}
	return;
}


static int _is_json_missing_entry(cJSON* array, const char* parent, const char* child)
{
	int n = cJSON_GetArraySize(array);
	char p[64], c[64];

	// start at beggining of array and check untill we find a match
	for(int i=0;i<n;i++){
		cJSON* item = cJSON_GetArrayItem(array, i);
		json_fetch_string(item, "parent", p, 64);
		json_fetch_string(item, "child", c, 64);
		if(strcmp(parent, p)==0 && strcmp(child, c)==0){
			//found!
			return 0;
		}
	}
	// no match, return failure
	return 1;
}

static void _add_default_extrinsic(cJSON* array, const char* parent_name, const char* child_name, double* t, double* r, int* counter)
{
	if(_is_json_missing_entry(array, parent_name, child_name)){
		printf("adding default extrinsics for %s to %s\n", parent_name, child_name);
		cJSON* item = cJSON_CreateObject();
		cJSON_AddStringToObject(item, "parent", parent_name);
		cJSON_AddStringToObject(item, "child", child_name);
		cJSON_AddItemToObject(item, "T_child_wrt_parent", cJSON_CreateDoubleArray(t, 3));
		cJSON_AddItemToObject(item, "RPY_parent_to_child", cJSON_CreateDoubleArray(r, 3));
		cJSON_AddItemToArray(array, item);
		json_set_modified_flag(1);
		*counter = *counter+1;
	}
	return;
}


int vcc_read_extrinsic_conf_file(const char* path, vcc_extrinsic_t* t, int* n, int max_t)
{
	// vars and defaults
	int i, m;

	//sanity checks
	if(t==NULL || n==NULL){
		fprintf(stderr, "ERROR in %s, received NULL pointer\n", __FUNCTION__);
		return -1;
	}
	if(max_t<2){
		fprintf(stderr, "ERROR in %s, maximum number of apriltags must be >=2\n", __FUNCTION__);
		return -1;
	}

	// check if the file exists and make a new one if not
	int ret = json_make_empty_file_with_header_if_missing(path, EXTRINSIC_CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Created new empty json file: %s\n", path);

	// read the data in
	cJSON* parent = json_read_file(path);
	if(parent==NULL) return -1;

	// file structure is just one big array of apriltag config structures
	cJSON* array = json_fetch_array_and_add_if_missing(parent, "extrinsics", &m);
	if(m > max_t){
		fprintf(stderr, "ERROR found %d extrinsics in file but maximum number of tags is set to %d\n", m, max_t);
		return -1;
	}

	// add defaults in
	double t0[] = {-0.0484,0.037,0.002};
	double r0[] = {0.0, 0.0, 0.0};
	_add_default_extrinsic(array, "imu1", "imu0", t0, r0, &m);

	double t1[] = {0.065,-0.014,0.013};
	double r1[] = {0.0, 45.0, 90.0};
	_add_default_extrinsic(array, "imu0", "tracking", t1, r1, &m);

	double t2[] = {0.017,0.015,0.013};
	double r2[] = {0.0, 45.0, 90.0};
	_add_default_extrinsic(array, "imu1", "tracking", t2, r2, &m);

	double t3[] = {0.020,0.014,-0.008};
	double r3[] = {0.0, 0.0, 0.0};
	_add_default_extrinsic(array, "body", "imu0", t3, r3, &m);

	double t4[] = {0.068,-0.015,-0.008};
	double r4[] = {0.0, 0.0, 0.0};
	_add_default_extrinsic(array, "body", "imu1", t4, r4, &m);

	double t5[] = {0.1,-0.04,0.0};
	double r5[] = {0.0,90.0,90.0};
	_add_default_extrinsic(array, "body", "stereo_l", t5, r5, &m);

	double t6[] = {0.1,0.0,0.0};
	double r6[] = {0.0,90.0,90.0};
	_add_default_extrinsic(array, "body", "tof", t6, r6, &m);

	double t7[] = {0.0,0.0,0.1};
	double r7[] = {0.0,0.0,0.0};
	_add_default_extrinsic(array, "body", "ground", t7, r7, &m);



	// Now actually parse the json data into our output array!!
	for(i=0; i<m; i++){
		cJSON* item = cJSON_GetArrayItem(array, i);
		json_fetch_string(		item, "parent",			t[i].parent, 64);
		json_fetch_string(		item, "child",			t[i].child,  64);
		json_fetch_fixed_vector(item,"T_child_wrt_parent",t[i].T_child_wrt_parent, 3);
		json_fetch_fixed_vector(item,"RPY_parent_to_child",t[i].RPY_parent_to_child, 3);
		// also write out the rotation matrix equivalent
		vcc_tait_bryan_xyz_degrees_to_rotation_matrix(t[i].RPY_parent_to_child, t[i].R_child_to_parent);
	}

	// check if we got any errors in that process
	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse data in %s\n", path);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		printf("The JSON apriltag data was modified during parsing, saving the changes to disk\n");
		json_write_to_file_with_header(path, parent, EXTRINSIC_CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);
	*n = m;
	return 0;
}


int vcc_find_extrinsic_in_array(const char* parent, const char* child, vcc_extrinsic_t* t, int n, vcc_extrinsic_t* out)
{
	// start at beggining of array and check untill we find a match
	for(int i=0;i<n;i++){
		// check if name and model match
		if(strcmp(t[i].parent, parent)==0 && strcmp(t[i].child, child)==0){
			// we are done, copy and return
			if(out!=NULL){
				memcpy(out, &t[i], sizeof(vcc_extrinsic_t));
			}
			return 0;
		}
	}
	// no match, return failure
	return -1;
}


void vcc_rotation_matrix_to_tait_bryan_xyz_degrees(double R[3][3], double tb[3])
{
	#ifndef RAD_TO_DEG
	#define RAD_TO_DEG (180.0/M_PI)
	#endif

	tb[1] = asin(R[0][2])*RAD_TO_DEG;
	if(fabs(R[0][2])<0.9999999){
		tb[0] = atan2(-R[1][2], R[2][2])*RAD_TO_DEG;
		tb[2] = atan2(-R[0][1], R[0][0])*RAD_TO_DEG;
	}
	else{
		tb[0] = atan2(-R[2][1], R[1][1])*RAD_TO_DEG;
		tb[2] = 0.0;
	}
	return;
}

void vcc_tait_bryan_xyz_degrees_to_rotation_matrix(double tb[3], double R[3][3])
{
	#ifndef DEG_TO_RAD
	#define DEG_TO_RAD (M_PI/180.0)
	#endif

	const double cx = cos(tb[0]*DEG_TO_RAD);
	const double sx = sin(tb[0]*DEG_TO_RAD);
	const double cy = cos(tb[1]*DEG_TO_RAD);
	const double sy = sin(tb[1]*DEG_TO_RAD);
	const double cz = cos(tb[2]*DEG_TO_RAD);
	const double sz = sin(tb[2]*DEG_TO_RAD);
	const double cxcz = cx * cz;
	const double cxsz = cx * sz;
	const double sxcz = sx * cz;
	const double sxsz = sx * sz;

	R[0][0] =  cy * cz;
	R[0][1] = -cy * sz;
	R[0][2] =  sy;
	R[1][0] =  cxsz + sxcz * sy;
	R[1][1] =  cxcz - sxsz * sy;
	R[1][2] = -sx * cy;
	R[2][0] =  sxsz - cxcz * sy;
	R[2][1] =  sxcz + cxsz * sy;
	R[2][2] =  cx * cy;

	return;
}


