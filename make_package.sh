#!/bin/bash
################################################################################
# Copyright (c) 2019 ModalAI, Inc. All rights reserved.
#
# creates an ipk package from compiled ros nodes.
# be sure to build everything first with build.sh in docker
# run this on host pc
# UPDATE VERSION IN CONTROL FILE, NOT HERE!!!
#
# author: james@modalai.com
################################################################################

set -e # exit on error to prevent bad ipk from being generated

################################################################################
# variables
################################################################################
VERSION=$(cat ipk/control/control | grep "Version" | cut -d' ' -f 2)
PACKAGE=$(cat ipk/control/control | grep "Package" | cut -d' ' -f 2)
IPK_NAME=${PACKAGE}_${VERSION}.ipk

DATA_DIR=ipk/data
CONTROL_DIR=ipk/control

echo ""
echo "Package Name: " $PACKAGE
echo "version Number: " $VERSION

################################################################################
# start with a little cleanup to remove old files
################################################################################
sudo rm -rf $DATA_DIR
sudo mkdir $DATA_DIR

sudo rm -rf ipk/control.tar.gz
sudo rm -rf ipk/data.tar.gz
sudo rm -rf $IPK_NAME

################################################################################
## Install everything into data directory.
## Do everything here with sudo since the ownership of every file and directory
## in the ipk data directory needs to match what it should be on VOXL.
################################################################################

DID_BUILD=false

if [[ -d "build" ]]; then
	cd build && sudo make DESTDIR=../ipk/data PREFIX=/usr install && cd -
	DID_BUILD=true
fi
if [[ -d "build32" ]]; then
	cd build32 && sudo make DESTDIR=../ipk/data PREFIX=/usr install && cd -
	DID_BUILD=true
fi
if [[ -d "build64" ]]; then
	cd build64 && sudo make DESTDIR=../ipk/data PREFIX=/usr install && cd -
	DID_BUILD=true
fi

# make sure at least one directory worked
if [ "$DID_BUILD" = false ]; then
	echo "neither build/ build32/ or build64/ were found"
	exit 1
fi

# also move all the scripts over
sudo cp scripts/voxl-configure-extrinsics.sh $DATA_DIR/usr/bin/voxl-configure-extrinsics
sudo chmod +x $DATA_DIR/usr/bin/voxl-configure-extrinsics
sudo cp scripts/voxl-configure-mpa.sh $DATA_DIR/usr/bin/voxl-configure-mpa
sudo chmod +x $DATA_DIR/usr/bin/voxl-configure-mpa

sudo mkdir -p $DATA_DIR/usr/share/modalai/extrinsic_configs/
sudo cp extrinsic_configs/* $DATA_DIR/usr/share/modalai/extrinsic_configs/

################################################################################
# pack the control, data, and final ipk archives
################################################################################

cd $CONTROL_DIR/
tar --create --gzip -f ../control.tar.gz *
cd ../../

cd $DATA_DIR/
tar --create --gzip -f ../data.tar.gz *
cd ../../

ar -r $IPK_NAME ipk/control.tar.gz ipk/data.tar.gz ipk/debian-binary

echo ""
echo DONE