/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

#define CLIENT_NAME			"voxl-inspect-pose"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int newline = 0;


#define DEG_TO_RAD	(3.14159265358979323846/180.0)
#define RAD_TO_DEG	(180.0/3.14159265358979323846)


// copied from voxl-vision-px4.h to avoid circular dependency since vvpx4 also
// is dependent on voxl-mpa-tools
#define BODY_WRT_LOCAL_POSE_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_body_wrt_local/")
#define BODY_WRT_FIXED_POSE_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_body_wrt_fixed/")


static void _print_usage(void)
{
	printf("\n\
Tool to print pose data to the screen for inspection.\n\
The --local and --fixed arguments are shortcuts to view the local and fixed\n\
frame poses published by voxl-vision-px4. However, any 6-dof pose pipe can\n\
be inspected.\n\
\n\
-f, --fixed             print pose of body with respect to fixed frame\n\
-h, --help              print this help message\n\
-l, --local             print pose of body with respect to lcoal frame\n\
-n, --newline           print newline between each pose\n\
\n\
typical usage:\n\
/# voxl-inspect-pose -l\n\
/# voxl-inspect-pose vvpx4_body_wrt_local\n\
/# voxl-inspect-pose -f\n\
/# voxl-inspect-pose vvpx4_body_wrt_fixed\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"fixed",			no_argument,		0,	'f'},
		{"help",			no_argument,		0,	'h'},
		{"local",			no_argument,		0,	'l'},
		{"newline",			no_argument,		0,	'n'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "fhln", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'f':
			if(pipe_path[0]!=0){
				fprintf(stderr, "ERROR: Please specify only one pose pipe\n");
				_print_usage();
				exit(1);
			}
			strcpy(pipe_path, BODY_WRT_FIXED_POSE_PATH);
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 'l':
			if(pipe_path[0]!=0){
				fprintf(stderr, "ERROR: Please specify only one pose pipe\n");
				_print_usage();
				exit(1);
			}
			strcpy(pipe_path, BODY_WRT_LOCAL_POSE_PATH);
			break;
		case 'n':
			newline = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_client_construct_full_path(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}



/*
 * Convert from Rotation matrix representing transformation from
 * frame 2 to frame 1.
 * The result will hold the angles defining the 3-2-1 intrinsic
 * Tait-Bryan rotation sequence from frame 1 to frame 2.
 * This is the usual nautical/aerospace order
 */
static void _rotation_to_tait_bryan(float R[3][3], float* roll, float* pitch, float* yaw)
{
	*roll  = atan2(R[2][1], R[2][2]);
	*pitch = asin(-R[2][0]);
	*yaw   = atan2(R[1][0], R[0][0]);

	if(fabs((double)*pitch - M_PI_2) < 1.0e-3){
		*roll = 0.0;
		*pitch = atan2(R[1][2], R[0][2]);
	}
	else if(fabs((double)*pitch + M_PI_2) < 1.0e-3) {
		*roll = 0.0;
		*pitch = atan2(-R[1][2], -R[0][2]);
	}
	return;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);
	printf("timestamp(ms)|");
	printf("     Position (m)     |");
	printf(" Roll Pitch Yaw (deg) |");
	printf("    Velocity (m/s)    |");
	printf(" angular rate (deg/s) |\n");
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// callback for simple helper when data is ready
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	pose_vel_6dof_t* d = modal_pose_vel_6dof_validate_pipe_data(data, bytes, &n_packets);

	// if there was an error OR no packets received, just return;
	if(d == NULL) return;
	if(n_packets<=0) return;

	// loop through each
	for(int i=0;i<n_packets;i++){

		if(!newline) printf("\r");

		// convert rotation to tait-bryan angles in degrees for easier viewing
		float roll, pitch, yaw;
		_rotation_to_tait_bryan(d[i].R_child_to_parent, &roll, &pitch, &yaw);
		roll	*= (float)RAD_TO_DEG;
		pitch	*= (float)RAD_TO_DEG;
		yaw		*= (float)RAD_TO_DEG;

		// print everything in one go.
		printf("%12ld | %6.2f %6.2f %6.2f | %6.1f %6.1f %6.1f | %6.2f %6.2f %6.2f | %6.2f %6.2f %6.2f |",\
		d[i].timestamp_ns/1000000,\
		(double)d[i].T_child_wrt_parent[0],\
		(double)d[i].T_child_wrt_parent[1],\
		(double)d[i].T_child_wrt_parent[2],\
		(double)roll, (double)pitch, (double)yaw,\
		(double)d[i].v_child_wrt_parent[0],\
		(double)d[i].v_child_wrt_parent[1],\
		(double)d[i].v_child_wrt_parent[2],\
		(double)d[i].w_child_wrt_child[0]* RAD_TO_DEG,\
		(double)d[i].w_child_wrt_child[1]* RAD_TO_DEG,\
		(double)d[i].w_child_wrt_child[2]* RAD_TO_DEG);

		if(newline) printf("\n");
		fflush(stdout);
	}
	return;
}


int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// normal non-test mode
	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_init_channel(0, pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
										POSE_6DOF_RECOMMENDED_READ_BUF_SIZE);
	if(ret){
		fprintf(stderr, "ERROR: failed to open pipe %s\n", pipe_path);
		pipe_client_print_error(ret);
		fprintf(stderr, "Either it is not enabled or voxl-vision-px4 is not running\n");
		return -1;
	}

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_client_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
