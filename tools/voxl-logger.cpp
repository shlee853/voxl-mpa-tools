/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <sstream>
#include <unistd.h>
#include <unistd.h>		// for access()
#include <sys/stat.h>	// for mkdir
#include <sys/types.h>
#include <limits.h>		// for PATH_MAX
#include <getopt.h>
#include <time.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <modal_pipe_client.h>
#include <modal_start_stop.h>
#include <modal_pipe_interfaces.h>
#include <modal_json.h>

#include "common.h"
#include "log_defs.h"

using namespace std;
using namespace cv;


#define PIPE_CLIENT_NAME		"voxl-logger"	// client name for pipes


typedef struct channel_t{
	int running;
	int type;
	pthread_t thread_id;
	char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
	int sample_limit;
	int n_samples;
	int n_to_skip;
	int n_skipped;
	FILE* csv_fd;
} channel_t;

// array of each channel state
static channel_t c[PIPE_CLIENT_MAX_CHANNELS];

// optional user-defined note to be included in the log json file
#define MAX_NOTE_LEN	128
static char note[MAX_NOTE_LEN] = "na";

static char base_dir[128] = LOG_BASE_DIR;
static char info_path[128];

// this is the complete path beginning with base_dir into which all log data
// in included, for example: /data/voxl-logger/log0001/
static char log_dir[512];

// number of channels enabled by user
static int n_ch;

// optional debug mode
static int en_debug;

// timeout set positive by command line arg indicates timeout feature is on
static float timeout_s = -1;
static pthread_t timeout_thread;
int64_t start_time;

// json objects
cJSON* parent;
cJSON* channel_array;
cJSON* duration_json;


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
Tool to save data published through Modal Pipe Architecture to disk.\n\
By default, this saves images to /data/voxl-logger/ since the /data/ parition is\n\
the largest partition on VOXL. You can change this with the -d argument.\n\
\n\
-c, --cam {name}            name of a camera to log, e.g. tracking\n\
-d, --directory {dir}       name of the directory to save the log to, default\n\
                              is /data/voxl-logger/ if argument is not provided.\n\
-i, --imu {name}            name of an imu to log, e.g. imu1.\n\
-k, --skip {n_to_skip}      number of samples to skip between logged samples\n\
-h, --help                  print this help message\n\
-n, --note {note}           optionally add a very short note to the end of the log\n\
                              directory name, e.g. flight_with_new_motors\n\
-o, --preset_odometry       record a preset log containing qvio, tracking cam and both\n\
                              IMUs for testing VIO. This can be used with other pipes.\n\
-s, --samples {samples}     maximum samples to save for one specific channel,\n\
                              otherwise it will run untill you quit the program.\n\
-t, --time {seconds}        time to log for in seconds.\n\
-v, --vio {name}            name of a vio pipe to log, e.g. qvio\n\
-z, --debug                 enable verbose debug mode\n\
\n\
typical uses:\n\
  To log one camera image:\n\
    yocto:/# voxl-logger --cam tracking --samples 1 \n\
\n\
  To log 5000 imu samples:\n\
    yocto:/# voxl-logger -i imu1 -s 5000 --note \"primary imu test\"\n\
\n\
  To log 1000 imu0 samples and 2000 imu1 samples:\n\
    yocto:/# voxl-logger -i imu0 -s 1000 -i imu1 -s 2000\n\
\n\
  To log every 5th imu sample (skip 4) for 5.5 seconds:\n\
    yocto:/# voxl-logger -i imu1 --skip 4 -t 5.5\n\
\n\
  To log tracking camera and both imus until you press ctrl-c\n\
    yocto:/# voxl-logger --preset_odometry\n\
\n\
  To record a typical log for testing VIO (imu + tracking) for 1 minute\n\
    yocto:/# voxl-logger --preset_odometry --time 60 --note \"log for vio replay test 1\"\n\
\n\
\n");
	return;
}



static int _mkdir(const char *dir)
{
	char tmp[PATH_MAX];
	char* p = NULL;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	for(p = tmp + 1; *p!=0; p++){
		if(*p == '/'){
			*p = 0;
			if(mkdir(tmp, S_IRWXU) && errno!=EEXIST){
				perror("ERROR calling mkdir");
				printf("tried to mkdir %s\n", tmp);
				return -1;
			}
			*p = '/';
		}
	}
	return 0;
}


static int _add_channel(int type, char* optarg)
{
	// start a new channel and log that we have a new channel enabled
	if(n_ch>=PIPE_CLIENT_MAX_CHANNELS){
		fprintf(stderr, "Too many channels requested!\n");
		return -1;
	}
	if(pipe_client_construct_full_path(optarg, c[n_ch].pipe_path)<0){
		fprintf(stderr, "Invalid pipe name: %s\n", optarg);
		return -1;
	}

	// check if the path has already been added
	for(int i=0; i<n_ch; i++){
		if(strcmp(c[n_ch].pipe_path, c[i].pipe_path)==0){
			printf("Warning: the following pipe was requested multiple times:\n");
			printf("%s\n", c[n_ch].pipe_path);
			return 0;
		}
	}

	c[n_ch].type = type;
	n_ch++;
	return 0;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"cam",             required_argument, 0, 'c'},
		{"directory",       required_argument, 0, 'd'},
		{"imu",             required_argument, 0, 'i'},
		{"skip",            required_argument, 0, 'k'},
		{"help",            no_argument,       0, 'h'},
		{"note",            required_argument, 0, 'n'},
		{"preset_odometry", no_argument,       0, 'o'},
		{"samples",         required_argument, 0, 's'},
		{"time",            required_argument, 0, 't'},
		{"vio",             required_argument, 0, 'v'},
		{"debug",           no_argument,       0, 'z'},
		{0, 0, 0, 0}
	};

	// we are going to set up the channels in sequence based on the order of
	// arguments as the user provided them. Start at 0 indicating the user
	// hasn't specified any channels yet. Once the first channel starts, this
	// will increase

	while(1){
		int len, n, notelen;
		int option_index = 0;
		int j = getopt_long(argc, argv, "c:d:i:k:hn:os:t:v:z", long_options, &option_index);

		if(j == -1) break; // Detect the end of the options.

		switch(j){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			if(_add_channel(TYPE_CAM, optarg)<0){
				return -1;
			}
			break;

		case 'd':
			// make the directory if necessary
			if(_mkdir(optarg)) return -1;
			sprintf(base_dir,"%s", optarg);
			len = strlen(base_dir);
			// make sure directory ends in a trailing '/'
			if(base_dir[len]!='/'){
				base_dir[len]='/';
				base_dir[len+1]=0;
			}
			break;

		case 'i':
			if(_add_channel(TYPE_IMU, optarg)<0){
				return -1;
			}
			break;

		case 'k':
			if(n_ch<1){
				fprintf(stderr, "ERROR: you must specify the skip arg after the respective pipe name\n");
				return -1;
			}
			n = atoi(optarg);
			if(n<0){
				fprintf(stderr, "ERROR: number of samples to skip must be >=0\n");
				return -1;
			}
			c[n_ch-1].n_to_skip = n;
			break;

		case 'h':
			_print_usage();
			return -1;
			break;

		case 'n':
			notelen = strlen(optarg);
			if(notelen<1){
				fprintf(stderr, "ERROR: empty note string provided\n");
				return -1;
			}
			if(notelen>=MAX_NOTE_LEN){
				fprintf(stderr, "ERROR: note string must be < %d characters\n", MAX_NOTE_LEN);
				return -1;
			}
			if(strchr(optarg, '/')!=NULL){
				fprintf(stderr, "ERROR: note string can't contain '/'\n");
				return -1;
			}
			// replace all spaces in the note with an underscore
			for(int c=0;c<notelen;c++){
				if(optarg[c]==' ') optarg[c] = '_';
			}
			strcpy(note,optarg);
			break;

		case 'o':
			if(_add_channel(TYPE_IMU, (char*)"imu0")<0){
				return -1;
			}
			if(_add_channel(TYPE_IMU, (char*)"imu1")<0){
				return -1;
			}
			if(_add_channel(TYPE_CAM, (char*)"tracking")<0){
				return -1;
			}
			if(_add_channel(TYPE_VIO, (char*)"qvio")<0){
				return -1;
			}
			break;

		case 's':
			if(n_ch<1){
				fprintf(stderr, "ERROR: you must specify the samples arg after the respective pipe name\n");
				return -1;
			}
			n = atoi(optarg);
			if(n<1){
				fprintf(stderr, "ERROR: number of samples must be positive\n");
				return -1;
			}
			c[n_ch-1].sample_limit = n;
			break;

		case 't':
			timeout_s = atof(optarg);
			if(timeout_s<0.5f){
				fprintf(stderr, "ERROR: timeout must be >=0.5 seconds\n");
				return -1;
			}
			break;

		case 'v':
			if(_add_channel(TYPE_VIO, optarg)<0){
				return -1;
			}
			break;

		case 'z':
			printf("enabling debug mode\n");
			en_debug = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	if(n_ch<1){
		fprintf(stderr, "ERROR: at least one pipe channel must be specified\n");
		return -1;
	}

	if(en_debug) printf("user requested %d channels\n", n_ch);

	return 0;
}


static int _start_csv(int ch)
{
	// construct the data path
	char csv_path[512];
	sprintf(csv_path, "%s%sdata.csv", log_dir, c[ch].pipe_path);
	if(en_debug){
		printf("making new csv file for channel %d: %s\n", ch, csv_path);
	}

	// make any required subdirs and the csv itself
	_mkdir(csv_path);
	FILE* fd = fopen(csv_path, "w+");
	if(fd == 0){
		fprintf(stderr, "ERROR: can't open log file for writing\n");
		c[ch].running = 0;
		return -1;
	}

	// write header
	int ret = 0;
	if(c[ch].type == TYPE_IMU){
		ret = fprintf(fd, "i,timestamp(ns),AX(m/s2),AY(m/s2),AZ(m/s2),GX(rad/s),GY(rad/s),GZ(rad/s),T(C)\n");
	}
	else if(c[ch].type == TYPE_CAM){
		ret = fprintf(fd, "i,timestamp(ns),gain,exposure(ns),format,height,width\n");
	}
	else if(c[ch].type == TYPE_VIO){
		fprintf(fd, "i,timestamp(ns),");
		fprintf(fd, "T_imu_wrt_vio_x(m),T_imu_wrt_vio_y(m),T_imu_wrt_vio_z(m),");
		fprintf(fd, "roll(rad),pitch(rad),yaw(rad),");
		fprintf(fd, "vel_imu_wrt_vio_x(m/s),vel_imu_wrt_vio_y(m/s),vel_imu_wrt_vio_z(m/s),");
		fprintf(fd, "angular_vel_x(rad/s),angular_vel_y(rad/s),angular_vel_z(rad/s),");
		fprintf(fd, "gravity_vector_x(m/s2),gravity_vector_y(m/s2),gravity_vector_z(m/s2),");
		fprintf(fd, "T_cam_wrt_imu_x(m),T_cam_wrt_imu_y(m),T_cam_wrt_imu_z(m),");
		fprintf(fd, "imu_to_cam_roll_x(rad),imu_to_cam_pitch_y(rad),imu_to_cam_yaw_z(rad),");
		ret = fprintf(fd, "features,quality,state,error_code\n");
	}

	if(ret<0){
		perror("failed to write csv header");
		return -1;
	}

	// save fd for writing to later
	c[ch].csv_fd = fd;
	return 0;
}

static void _simple_cb(int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// reading from a pipe will return 0 if the other end (server) closes
	// check for this (and negative  numbers indicating error) and quit.
	// your program doesn't need to quit, you can handle the server quitting
	// however you like to suite your needs. We just quit for this example.
	if(bytes<=0){
		fprintf(stderr, "Server Disconnected from channel %d\n", ch);
		c[ch].running = 0;
		return;
	}

	// if we've been told to stop running, just return
	if(c[ch].running==0 || main_running==0) return;

	// if file hasn't been updated yet, open it!
	if(c[ch].csv_fd==0){
		if(_start_csv(ch)){
			return;
		}
	}

	// validate that the data makes sense
	int n_received;
	imu_data_t* imu_array;
	vio_data_t* vio_array;

	if(c[ch].type == TYPE_IMU){
		imu_array = modal_imu_validate_pipe_data(data, bytes, &n_received);
		if(en_debug){
			printf("read %d IMU packets from ch: %d\n", n_received, ch);
		}
		if(imu_array == NULL) return;
		if(n_received<=0) return;
	}
	else if(c[ch].type == TYPE_VIO){
		vio_array = modal_vio_validate_pipe_data(data, bytes, &n_received);
		if(en_debug){
			printf("read %d VIO packets from ch: %d\n", n_received, ch);
		}
		if(vio_array == NULL) return;
		if(n_received<=0) return;
	}
	else{
		fprintf(stderr, "ERROR in simple callback, unknown type\n");
	}

	// if we are printing every packet, loop through each
	for(int i=0;i<n_received;i++){

		// make sure we don't need to quit
		if(!main_running) return;

		// skip samples if necessary
		if(c[ch].n_to_skip>0){
			if(c[ch].n_skipped>=c[ch].n_to_skip){
				c[ch].n_skipped = 0;
			}
			else{
				c[ch].n_skipped++;
				if(en_debug){
					printf("skipping sample ch: %d n_skipped: %d, n_to_skip: %d", ch, c[ch].n_skipped, c[ch].n_to_skip);
				}
				continue;
			}
		}

		// print everything in one go.
		int ret = 0;
		if(c[ch].type == TYPE_IMU){
			ret = fprintf(c[ch].csv_fd, "%d,%ld,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.5f,%0.2f\n",\
			c[ch].n_samples, imu_array[i].timestamp_ns,\
			(double)imu_array[i].accl_ms2[0],(double)imu_array[i].accl_ms2[1],(double)imu_array[i].accl_ms2[2],\
			(double)imu_array[i].gyro_rad[0],(double)imu_array[i].gyro_rad[1],(double)imu_array[i].gyro_rad[2],\
			(double)imu_array[i].temp_c);
		}
		else if(c[ch].type == TYPE_VIO){
			vio_data_t d = vio_array[i];
			float roll, pitch, yaw, roll_cam, pitch_cam, yaw_cam;
			_rotation_to_tait_bryan(d.R_imu_to_vio, &roll, &pitch, &yaw);
			_rotation_to_tait_bryan_xyz_intrinsic(d.R_cam_to_imu, &roll_cam, &pitch_cam, &yaw_cam);
			fprintf(c[ch].csv_fd, "%d,%ld,",c[ch].n_samples, vio_array[i].timestamp_ns);
			fprintf(c[ch].csv_fd, "%0.4f,%0.4f,%0.4f,", (double)d.T_imu_wrt_vio[0], (double)d.T_imu_wrt_vio[1], (double)d.T_imu_wrt_vio[2]);
			fprintf(c[ch].csv_fd, "%0.4f,%0.4f,%0.4f,", (double)roll, (double)pitch, (double)yaw);
			fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)d.vel_imu_wrt_vio[0], (double)d.vel_imu_wrt_vio[1], (double)d.vel_imu_wrt_vio[2]);
			fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)d.imu_angular_vel[0], (double)d.imu_angular_vel[1], (double)d.imu_angular_vel[2]);
			fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)d.gravity_vector[0], (double)d.gravity_vector[1], (double)d.gravity_vector[2]);
			fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)d.T_cam_wrt_imu[0], (double)d.T_cam_wrt_imu[1], (double)d.T_cam_wrt_imu[2]);
			fprintf(c[ch].csv_fd, "%0.3f,%0.3f,%0.3f,", (double)roll_cam, (double)pitch_cam, (double)yaw_cam);
			ret = fprintf(c[ch].csv_fd, "%d,%0.5f,%d,%d\n", d.n_feature_points,(double)d.quality, d.state, d.error_code);
		}
		else{
			fprintf(stderr, "ERROR in simple callback, unknown type\n");
		}

		// check for write errors
		if(ret<0){
			perror("ERROR writing to disk");
			c[ch].running = 0;
		}

		// increment counter and check if we are finished
		c[ch].n_samples++;
		if(c[ch].sample_limit>0 && c[ch].n_samples>=c[ch].sample_limit){
			c[ch].running = 0;
			printf("channel %d reached sample limit, stopping itself\n", ch);
			return;
		}
	}

	// sleep a bit so we don't thrash the disk. The pipe will handle buffering
	// the data while we sleep.
	usleep(100000);

	return;
}


static void _construct_cam_path(int ch, int i, char* path)
{
	sprintf(path, "%s%s%05d.png", log_dir, c[ch].pipe_path, i);
	return;
}

static void _construct_stereo_path(int ch, int i, char* path_l, char* path_r)
{
	sprintf(path_l, "%s%s%05dl.png", log_dir, c[ch].pipe_path, i);
	sprintf(path_r, "%s%s%05dr.png", log_dir, c[ch].pipe_path, i);
	return;
}

// camera frame callback registered to voxl-camera-server
static void _cam_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, char* frame, __attribute__((unused)) void* context)
{
	// if we've been told to stop running, just return
	if(c[ch].running==0) return;

	// if file hasn't been updated yet, open it!
	if(c[ch].csv_fd==0){
		if(_start_csv(ch)){
			return;
		}
	}

	// skip samples if necessary
	if(c[ch].n_to_skip>0){
		if(c[ch].n_skipped>=c[ch].n_to_skip){
			c[ch].n_skipped = 0;
		}
		else{
			c[ch].n_skipped++;
			if(en_debug){
				printf("skipping camera frame ch: %d n_skipped: %d, n_to_skip: %d", ch, c[ch].n_skipped, c[ch].n_to_skip);
			}
			return;
		}
	}


	int w = meta.width;
	int h = meta.height;
	char img_path1[512];
	char img_path2[512];

	// make opencv Mat images and write to disk
	if(meta.format == IMAGE_FORMAT_RAW8){
		Mat img1(h,w,CV_8UC1,frame);
		_construct_cam_path(ch, c[ch].n_samples, img_path1);
		imwrite(img_path1, img1);
	}
	else if(meta.format == IMAGE_FORMAT_STEREO_RAW8){
		Mat img1(h,w,CV_8UC1,frame);
		Mat img2(h,w,CV_8UC1,frame+(h*w));
		_construct_stereo_path(ch, c[ch].n_samples, img_path1, img_path2);
		imwrite(img_path1, img1);
		imwrite(img_path2, img2);
	}
	else if(meta.format == IMAGE_FORMAT_RAW16){
		Mat img1(h,w,CV_16SC1,frame);
		_construct_cam_path(ch, c[ch].n_samples, img_path1);
		imwrite(img_path1, img1);
	}
	else if(meta.format == IMAGE_FORMAT_FLOAT32){
		Mat img1(h,w,CV_32FC1,frame);
		Mat img1_8;
		// who knows what the range of numbers will be for floating point inputs
		// so scale it with minmax method to 0-255 for normal 8-bit output
		// TODO how to do this such that it can be replayed?
		normalize(img1, img1_8, 0, 255, NORM_MINMAX, CV_8UC1, noArray());
		_construct_cam_path(ch, c[ch].n_samples, img_path1);
		imwrite(img_path1, img1_8);
	}
	else{
		fprintf(stderr, "ERROR only support RAW8, RAW16, and FLOAT32 images right now\n");
		fprintf(stderr, "got %s instead\n", modal_image_format_name(meta.format));
		return;
	}

	// print metadata to csv
	int ret = fprintf(c[ch].csv_fd, "%d,%ld,%d,%d,%d,%d,%d\n",\
	c[ch].n_samples, meta.timestamp_ns, meta.gain, meta.exposure_ns, meta.format, h, w);

	// check for write errors
	if(ret<0){
		perror("ERROR writing to disk");
		c[ch].running = 0;
	}

	// increment counter and check if we are finished
	c[ch].n_samples++;
	if(c[ch].sample_limit>0 && c[ch].n_samples>=c[ch].sample_limit){
		c[ch].running = 0;
		printf("channel %d reached sample limit, stopping itself\n", ch);
		return;
	}

	return;
}


// servers may not necessarily be up and running when the logger starts, so keep
// trying untill they are.
// TODO remove this in favor of AUTO_RECONNECT mode
static void* _pipe_opener(void* arg)
{
	int ret;
	int ch = (long)arg;

	// keep trying to open the requested pipe as long as main is still running
	// and the helper thread hasn't closed itself
	while(main_running && c[ch].running){

		if(c[ch].type == TYPE_IMU){
			// attempt to connect to the server expecting it might fail
			ret = pipe_client_init_channel(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_SIMPLE_HELPER, IMU_RECOMMENDED_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to server
				printf("connected to ch%2d imu server: %s\n", ch, c[ch].pipe_path);
				pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
				return NULL;
			}
			if(en_debug) printf("failed to connect to server: %s trying again\n", c[ch].pipe_path);
		}

		else if(c[ch].type == TYPE_CAM){
			// attempt to connect to the server expecting it might fail
			ret = pipe_client_init_channel(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_CAMERA_HELPER, 0);
			if(ret == 0){
				// great! we connected to server
				printf("connected to ch%2d cam server: %s\n", ch, c[ch].pipe_path);
				pipe_client_set_camera_helper_cb(ch, _cam_cb, NULL);
				return NULL;
			}
			if(en_debug) printf("failed to connect to server: %s trying again\n", c[ch].pipe_path);
		}

		else if(c[ch].type == TYPE_VIO){
			// attempt to connect to the server expecting it might fail
			ret = pipe_client_init_channel(ch, c[ch].pipe_path, PIPE_CLIENT_NAME,
								EN_PIPE_CLIENT_SIMPLE_HELPER, VIO_RECOMMENDED_READ_BUF_SIZE);
			if(ret == 0){
				// great! we connected to server
				printf("connected to ch%2d vio server: %s\n", ch, c[ch].pipe_path);
				pipe_client_set_simple_helper_cb(ch, _simple_cb, NULL);
				return NULL;
			}
			if(en_debug) printf("failed to connect to server: %s trying again\n", c[ch].pipe_path);
		}

		else{
			fprintf(stderr, "ERROR in _pipe_opener unsupported type\n");
			return NULL;
		}
		usleep(100000);
	}

	return NULL;
}


static void _update_json(int update_time)
{
	if(en_debug) printf("updating json file\n");

	if(update_time){
		// update running duration
		int64_t duration_ns = _time_monotonic_ns() - start_time;
		cJSON_SetNumberValue(duration_json, (double)duration_ns/1000000000.0);
	}

	int i;
	for(i=0;i<n_ch;i++){
		cJSON* array_item = cJSON_GetArrayItem(channel_array, i);
		if(array_item==NULL){
			fprintf(stderr, "ERROR, can't find channel array item in json\n");
		}
		cJSON* sample_obj = cJSON_GetObjectItem(array_item, "n_samples");
		if(array_item==NULL){
			fprintf(stderr, "ERROR, can't find n_samples item in json\n");
		}
		cJSON_SetIntValue(sample_obj, c[i].n_samples);
	}
	// finish writing out final totals for samples
	if(en_debug){
		printf("writing info.json to disk\n");
	}
	json_write_to_file(info_path, parent);
	return;
}


// pthread that stops the program after time
static void* _stop_timer(__attribute__((unused)) void* context)
{
	printf("starting %0.1f second timer\n", (double)timeout_s);
	usleep(timeout_s*1000000);
	printf("stopping automatically after requested timeout\n");
	main_running = 0;
	return NULL;
}

int main(int argc, char* argv[])
{
	int i;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// start by making sure the base directory exists
	if(en_debug) printf("making sure base directory exists:%s\n", base_dir);
	_mkdir(base_dir);

	// search for existing log folders to determine the next number in the series
	for(i=0; i<=10000; i++){

		// make the next log directory in the sequence
		sprintf(log_dir, "%slog%04d/", base_dir, i);
		if(en_debug) printf("check if previous log dir exists: %s\n", log_dir);

		// use basic mkdir to try making this directory
		int ret = mkdir(log_dir, S_IRWXU);
		if(ret==0){
			if(en_debug) printf("successfully made new dir: %s\n", log_dir);
			break;
		}
		if(errno!=EEXIST){
			perror("error searching for next log directory in sequence");
			return -1;
		}
		// dir exists, move onto the next one
	}

	// make a json info file to describe whats contained in this log
	if(en_debug) printf("starting json object\n");
	parent = cJSON_CreateObject();
	if(parent==NULL) return -1;
	// start by recording that we are on log format version 1. In the future
	// if we need to change the format we can use this to differentiate
	cJSON_AddNumberToObject(parent, "log_format_version", 1);
	// put user-specified note and the total number of channels at the top
	cJSON_AddStringToObject(parent, "note", note);
	cJSON_AddNumberToObject(parent, "n_channels", n_ch);
	start_time = _time_monotonic_ns();
	cJSON_AddNumberToObject(parent, "start_time_monotonic_ns", start_time);

	char datestring[128];
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	sprintf(datestring,"%d-%02d-%02d %02d:%02d:%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	cJSON_AddStringToObject(parent, "start_time_date", datestring);
	duration_json = cJSON_AddNumberToObject(parent, "duration_s", 0.0);

	// create an array of objects describing each channel
	channel_array = cJSON_CreateArray();
	cJSON_AddItemToObject(parent, "channels", channel_array);
	for(i=0;i<n_ch;i++){
		if(en_debug) printf("adding ch %d to json\n", i);
		cJSON* new_channel = cJSON_CreateObject();
		cJSON_AddNumberToObject(new_channel, "channel", i);
		cJSON_AddNumberToObject(new_channel, "n_samples", -1); // -1 indicates unknown
		cJSON_AddNumberToObject(new_channel, "n_to_skip", c[i].n_to_skip);
		cJSON_AddNumberToObject(new_channel, "type", c[i].type);
		cJSON_AddStringToObject(new_channel, "type_string", _type_to_string(c[i].type));
		cJSON_AddStringToObject(new_channel, "pipe_path", c[i].pipe_path);
		// put the final object into the array
		cJSON_AddItemToArray(channel_array, new_channel);
	}

	// write out to disk with starting values for now. The parent cJSON object
	// will be updated as we go and the final numbers will be re-written to disk
	sprintf(info_path, "%sinfo.json", log_dir);
	if(en_debug) printf("writing json to file: %s\n", info_path);
	if(json_write_to_file(info_path, parent)) return -1;

	// copy over config and calibration files
	char cmd[512];
	sprintf(cmd, "%sdata/modalai/", log_dir);
	_mkdir(cmd);
	sprintf(cmd, "cp -R /data/modalai/*.yml %sdata/modalai/", log_dir);
	system(cmd);
	sprintf(cmd, "%setc/modalai/", log_dir);
	_mkdir(cmd);
	sprintf(cmd, "cp -R /etc/modalai/*.conf %setc/modalai/", log_dir);
	system(cmd);


	// set main running flag to 1 so the threads about to be created don't just
	// exit right away
	main_running = 1;

	// start threads to open all the pipes
	for(i=0;i<n_ch;i++){
		c[i].running = 1;
		if(en_debug) printf("starting pipe opener thread for channel %d\n", i);
		pthread_create(&c[i].thread_id, NULL, _pipe_opener, (void*)(long)i);
	}

	// main updates the json file and decides when to close up
	// the running flag will only go from 1 to 0 if the channel thread closes
	// by itself due to error or reaching the sample limit, so we can check the
	// running flag to see if all of the channels closed themselves.

	if(en_debug) printf("entering main loop\n");

	if(timeout_s>0){
		pthread_create(&timeout_thread, NULL, _stop_timer, NULL);
	}
	while(main_running){

		// check for the condition where all the threads stopped themselves
		int have_all_stopped = 1;
		for(i=0;i<n_ch;i++){
			if(c[i].running!=0){
				have_all_stopped = 0;
				break;
			}
		}
		if(have_all_stopped){
			if(en_debug) printf("all helpers stopped themselves\n");
			break;
		}

		// wait a bit
		usleep(100000);

		// update the json file with running tally of samples
		_update_json(1);
	}


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	// wait a bit for opener threads to close and things to write to disk
	usleep(500000);

	// close all the pipes
	if(en_debug) printf("closing pipes\n");
	pipe_client_close_all();

	// flush and close all csv files
	if(en_debug) printf("flushing and closing csv files\n");
	for(i=0;i<n_ch;i++){
		if(c[i].csv_fd!=0){
			fflush(c[i].csv_fd);
			fclose(c[i].csv_fd);
		}
	}

	// final update of running sample totals to the json file
	_update_json(0);

	// print some helpful info to the screen
	printf("summary of data logged:\n");
	for(i=0;i<n_ch;i++){
		printf("ch%2d, type: %s, samples: %7d, path: %s\n", i, _type_to_string(c[i].type), c[i].n_samples, c[i].pipe_path);
	}

	printf("created new log here:\n%s\n", log_dir);
	return 0;
}

