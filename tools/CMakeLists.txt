

set(TARGET voxl-logger)
add_executable(${TARGET} voxl-logger.cpp)
include_directories(/usr/include/opencv4/)
target_link_libraries(${TARGET}
	${OpenCV_LIBS}
	/usr/lib64/libmodal_pipe.so
	/usr/lib64/libmodal_json.so
	pthread
)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-replay)
add_executable(${TARGET} voxl-replay.cpp)
include_directories(/usr/include/opencv4/)
target_link_libraries(${TARGET}
	${OpenCV_LIBS}
	/usr/lib64/libmodal_pipe.so
	/usr/lib64/libmodal_json.so
	pthread
)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-kill-pipe)
add_executable(${TARGET} voxl-kill-pipe.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-extrinsics)
add_executable(${TARGET} voxl-inspect-extrinsics.c)
include_directories(../lib/)
target_link_libraries(${TARGET} voxl_common_config)
list(APPEND ALL_TARGETS ${TARGET})

set(TARGET voxl-inspect-apriltag-config)
add_executable(${TARGET} voxl-inspect-apriltag-config.c)
include_directories(../lib/)
target_link_libraries(${TARGET} voxl_common_config)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-cam)
add_executable(${TARGET} voxl-inspect-cam.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-imu)
add_executable(${TARGET} voxl-inspect-imu.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so m)
list(APPEND ALL_TARGETS ${TARGET})

set(TARGET voxl-inspect-pose)
add_executable(${TARGET} voxl-inspect-pose.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so m)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-services)
add_executable(${TARGET} voxl-inspect-services.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so m)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-points)
add_executable(${TARGET} voxl-inspect-points.c)
target_link_libraries(${TARGET} /usr/lib64/libmodal_pipe.so m)
list(APPEND ALL_TARGETS ${TARGET})


set(TARGET voxl-inspect-vibration)
add_executable(${TARGET} voxl-inspect-vibration.c)
target_link_libraries(${TARGET}
	/usr/lib64/libmodal_pipe.so
	/usr/lib64/librc_math.so
	m
)
list(APPEND ALL_TARGETS ${TARGET})




install(
	TARGETS ${ALL_TARGETS}
	LIBRARY			DESTINATION /usr/lib
	RUNTIME			DESTINATION /usr/bin
	PUBLIC_HEADER	DESTINATION /usr/include
)
