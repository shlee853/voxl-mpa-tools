/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>

#include "common.h"

#define CLIENT_NAME			"voxl-inspect-imu"

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int en_all = 0;
static int en_newline = 0;


static void _print_usage(void)
{
	printf("\n\
Tool to print imu data to the screen for inspection.\n\
By default this will print the average of all packets received each read which\n\
is usually 5. This is done to limit spamming data to the screen since the IMUs\n\
usually sample very fast (1khz+). Use the --all option to print every single\n\
IMU sample received\n\
\n\
See also: voxl-inspect-vibration and voxl-calibrate-imu\n\
\n\
-a, --all                   print all data received\n\
-h, --help                  print this help message\n\
-n, --newline               print each sample on its own line\n\
\n\
example usage:\n\
/# voxl-inspect-imu imu1\n\
/# voxl-inspect-imu /run/mpa/imu0/\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"all",				no_argument,		0,	'a'},
		{"help",			no_argument,		0,	'h'},
		{"newline",			no_argument,		0,	'n'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "ahn", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'a':
			en_all = 1;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 'n':
			en_newline = 1;
			break;
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_client_construct_full_path(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL RESET_FONT);
	if(en_all){
		printf("\nAcc in m/s^2, gyro in rad/s, temp in C, time in ms\n\n");
		printf(FONT_BOLD);
		printf("gravity| accl_x accl_y accl_z| gyro_x gyro_y gyro_z|  Temp | timestamp |  dt  |\n");
	}
	else{
		printf("\nAcc in m/s^2, gyro in rad/s, temp in C\n\n");
		printf(FONT_BOLD);
		printf("gravity| accl_x accl_y accl_z| gyro_x gyro_y gyro_z|  Temp |\n");
	}
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// called when the simple helper has data for us
static void _helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	imu_data_t* data_array = modal_imu_validate_pipe_data(data, bytes, &n_packets);
	if(data_array == NULL) return;



	// keep track of last packet to calculate dt
	static uint64_t last_ts_ns = 0;
	int64_t dt;


	// if we are printing every packet, loop through each
	if(en_all){
		for(int i=0;i<n_packets;i++){
			// keep track of timestep
			if(last_ts_ns == 0) dt = 0;
			else dt = data_array[i].timestamp_ns - last_ts_ns;
			last_ts_ns = data_array[i].timestamp_ns;

			float magnitude = sqrtf( \
					(data_array[i].accl_ms2[0] * data_array[i].accl_ms2[0]) +\
					(data_array[i].accl_ms2[1] * data_array[i].accl_ms2[1]) +\
					(data_array[i].accl_ms2[2] * data_array[i].accl_ms2[2]));

			// print everything in one go.
			if(!en_newline) printf("\r" CLEAR_LINE);
			printf("%6.2f |%6.2f %6.2f %6.2f |%6.2f %6.2f %6.2f |%6.2f |%10ld | %5.3f| ",\
			(double)magnitude,\
			(double)data_array[i].accl_ms2[0],(double)data_array[i].accl_ms2[1],(double)data_array[i].accl_ms2[2],\
			(double)data_array[i].gyro_rad[0],(double)data_array[i].gyro_rad[1],(double)data_array[i].gyro_rad[2],\
			(double)data_array[i].temp_c, data_array[i].timestamp_ns/1000000, (double)dt/1000000.0);

			if(en_newline) printf("\n");
		}
	}

	// if not printing all data, print the average of each packet
	else{
		// make a new data struct to hold the average
		imu_data_t avg;
		memset(&avg,0,sizeof(avg));

		// sum all the samples
		for(int i=0;i<n_packets;i++){
			for(int j=0;j<3;j++){
				avg.accl_ms2[j] += data_array[i].accl_ms2[j];
				avg.gyro_rad[j] += data_array[i].gyro_rad[j];
			}
			avg.temp_c += data_array[i].temp_c;
		}

		// find average by dividing the sum
		for(int j=0;j<3;j++){
			avg.accl_ms2[j] /= n_packets;
			avg.gyro_rad[j] /= n_packets;
		}
		avg.temp_c /= n_packets;

		float magnitude = sqrtf( \
				(avg.accl_ms2[0] * avg.accl_ms2[0]) +\
				(avg.accl_ms2[1] * avg.accl_ms2[1]) +\
				(avg.accl_ms2[2] * avg.accl_ms2[2]));
		if(!en_newline) printf("\r" CLEAR_LINE);
		printf("%6.2f |%6.2f %6.2f %6.2f |%6.2f %6.2f %6.2f |%6.2f | ",\
		(double)magnitude,\
		(double)avg.accl_ms2[0],(double)avg.accl_ms2[1],(double)avg.accl_ms2[2],\
		(double)avg.gyro_rad[0],(double)avg.gyro_rad[1],(double)avg.gyro_rad[2],\
		(double)avg.temp_c);
		if(en_newline) printf("\n");
	}
	fflush(stdout);
	return;
}



int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// prints can be quite long, disable terminal wrapping
	printf(DISABLE_WRAP);

	// set up all our MPA callbacks
	pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_init_channel(0, pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_SIMPLE_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, \
				IMU_RECOMMENDED_READ_BUF_SIZE);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_client_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
