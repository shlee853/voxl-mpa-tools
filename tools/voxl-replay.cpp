/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <sstream>
#include <unistd.h>
#include <unistd.h>		// for access()
#include <sys/stat.h>	// for mkdir
#include <sys/types.h>
#include <limits.h>		// for PATH_MAX
#include <getopt.h>
#include <time.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/utility.hpp>

#include <modal_pipe_server.h>
#include <modal_start_stop.h>
#include <modal_pipe_interfaces.h>
#include <modal_json.h>

#include "common.h"
#include "log_defs.h"

using namespace std;
using namespace cv;


#define MIN_CAM_PIPE_SIZE	(16*1024*1024) // 16MiB for now

// log types, more to be added later!!
#define TYPE_IMU	1
#define TYPE_CAM	2
#define TYPE_VIO	3


typedef struct channel_t{
	int running;
	int type;
	pthread_t thread_id;
	char out_pipe_path[MODAL_PIPE_MAX_PATH_LEN];
	char log_pipe_path[MODAL_PIPE_MAX_PATH_LEN];
	int n_total; // total samples as reported by the log file
	int n_read; // number of sample read including skipped samples
	FILE* csv_fd;
} channel_t;

// array of each channel state
static channel_t c[PIPE_SERVER_MAX_CHANNELS];

// this is the complete path beginning with base_dir into which all log data
// in included, for example: /data/voxl-logger/log0001/
static char log_dir[512];
static char base_dir[128] = LOG_BASE_DIR;

static int n_ch;					// number of channels in log
static int en_debug;				// optional debug mode
static int64_t log_start_time_ns;	// start time when the log was created
static int64_t replay_start_time_ns;	// start time when the replay starts

// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
Replay a log\n\
\n\
-d, --debug                 enable verbose debug mode\n\
-h, --help                  print this help message\n\
-n, --number {number}       number of log to open, e.g. 32\n\
-p, --path {path}           complete path of log to open, e.g. /data/mylog/\n\
\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"debug",       no_argument,       0, 'd'},
		{"help",        no_argument,       0, 'h'},
		{"number",      required_argument, 0, 'n'},
		{"path",        required_argument, 0, 'p'},
		{0, 0, 0, 0}
	};

	// we are going to set up the channels in sequence based on the order of
	// arguments as the user provided them. Start at 0 indicating the user
	// hasn't specified any channels yet. Once the first channel starts, this
	// will increase

	while(1){
		int len;
		int option_index = 0;
		int j = getopt_long(argc, argv, "dhn:p:", long_options, &option_index);

		if(j == -1) break; // Detect the end of the options.

		switch(j){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'h':
			_print_usage();
			return -1;
			break;

		case 'd':
			printf("enabling debug mode\n");
			en_debug = 1;
			break;

		case 'n':
			if(log_dir[0]!=0){
				fprintf(stderr, "can't provide multiple logs to replay\n");
				return -1;
			}
			sprintf(log_dir, "%slog%04d/", base_dir, atoi(optarg));
			break;

		case 'p':
			if(log_dir[0]!=0){
				fprintf(stderr, "can't provide multiple logs to replay\n");
				return -1;
			}
			strcpy(log_dir,optarg);
			len = strlen(log_dir);
			if(len<1){
				fprintf(stderr, "provided log path too short\n");
				return -1;
			}
			if(log_dir[0]!='/'){
				fprintf(stderr, "log dirrectory path must be absolute and begin with '/'\n");
				return -1;
			}
			if(log_dir[len-1]!='/'){
				fprintf(stderr, "log dirrectory path must be absolute and end with '/'\n");
				return -1;
			}
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	if(log_dir[0]==0){
		fprintf(stderr, "ERROR: you must specify a log to replay\n");
		_print_usage();
		return -1;
	}

	return 0;
}


// convert the xyz intrinsic rotation sequence we use for camera extrinsics to
// a rotation matrix. units of radians
static void _tait_bryan_xyz_to_rotation_matrix(float tb[3], float R[3][3])
{
	const double cx = cos(tb[0]);
	const double sx = sin(tb[0]);
	const double cy = cos(tb[1]);
	const double sy = sin(tb[1]);
	const double cz = cos(tb[2]);
	const double sz = sin(tb[2]);
	const double cxcz = cx * cz;
	const double cxsz = cx * sz;
	const double sxcz = sx * cz;
	const double sxsz = sx * sz;

	R[0][0] =  cy * cz;
	R[0][1] = -cy * sz;
	R[0][2] =  sy;
	R[1][0] =  cxsz + sxcz * sy;
	R[1][1] =  cxcz - sxsz * sy;
	R[1][2] = -sx * cy;
	R[2][0] =  sxsz - cxcz * sy;
	R[2][1] =  sxcz + cxsz * sy;
	R[2][2] =  cx * cy;

	return;
}

// convert the zyx intrinsic rotation sequence we use for airframe rotation to
// a rotation matrix. units of radians
static void _tait_bryan_zyx_to_rotation_matrix(float tb[3], float R[3][3])
{
	double c1 = cos(tb[2]);
	double s1 = sin(tb[2]);
	double c2 = cos(tb[1]);
	double s2 = sin(tb[1]);
	double c3 = cos(tb[0]);
	double s3 = sin(tb[0]);

	R[0][0] = c1*c2;
	R[0][1] = (c1*s2*s3)-(c3*s1);
	R[0][2] = (s1*s3)+(c1*c3*s2);

	R[1][0] = c2*s1;
	R[1][1] = (c1*c3)+(s1*s2*s3);
	R[1][2] = (c3*s1*s2)-(c1*s3);

	R[2][0] = -s2;
	R[2][1] = c2*s3;
	R[2][2] = c2*c3;

	return;
}


static int64_t _correct_timestamp_ns(int64_t log_ts)
{
	int64_t ret = log_ts + (replay_start_time_ns - log_start_time_ns);
	if(ret<0){
		fprintf(stderr, "critical error calculating corrected timestamp\n");
		return -1;
	}
	return ret;
}

static void _wait_before_publish(int ch, int64_t publish_ts_ns)
{
	// see how long we need to wait before publishing this packet and sleep if
	// it's positive. If negative we are falling behind which is okay as long
	// as it's a little bit behind, real world data will always have delay.
	int64_t time_to_go = publish_ts_ns - _time_monotonic_ns();
	if(time_to_go>0){
		usleep(time_to_go/1000);
	}
	if(time_to_go<-100000000 && en_debug){
		fprintf(stderr, "Warning: channel %d falling behind\n", ch);
	}
	return;
}

// TODO, bundle up multiple imu samples since they are so fast.
static int _publish_imu(int ch, char* line)
{
	imu_data_t d;
	int ret;
	int index;
	int64_t timestamp_log;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%f,%f,%f,%f,%f,%f,%f\n",\
	&index, &timestamp_log,\
	&d.accl_ms2[0],&d.accl_ms2[1],&d.accl_ms2[2],\
	&d.gyro_rad[0],&d.gyro_rad[1],&d.gyro_rad[2],\
	&d.temp_c);

	d.magic_number = IMU_MAGIC_NUMBER;

	// make sure all fields were populated
	if(ret!=9){
		fprintf(stderr, "failed to parse IMU csv line %d for channel %d\n", c[ch].n_read+1, ch);
		perror("error:");
		return -1;
	}

	// correct the timestamp to line up with current time and wait if necessary
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus an IMU packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}
	pipe_server_send_to_channel(ch, (char*)&d, sizeof(d));

	return 0;
}


static void _construct_cam_path(int ch, int i, char* path)
{
	sprintf(path, "%s%s%05d.png", log_dir, c[ch].out_pipe_path, i);
	return;
}

static void _construct_stereo_path(int ch, int i, char* path_l, char* path_r)
{
	sprintf(path_l, "%s%s%05dl.png", log_dir, c[ch].out_pipe_path, i);
	sprintf(path_r, "%s%s%05dr.png", log_dir, c[ch].out_pipe_path, i);
	return;
}

static int _publish_cam(int ch, char* line)
{
	camera_image_metadata_t d;
	int ret;
	int index;
	int64_t timestamp_log;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%hd,%d,%hd,%hd,%hd\n",\
	&index, &timestamp_log,\
	&d.gain, &d.exposure_ns, &d.format, &d.height, &d.width);
	d.magic_number = CAMERA_MAGIC_NUMBER;
	d.frame_id = index;
	d.reserved = 0;

	// make sure all fields were populated
	if(ret!=7){
		fprintf(stderr, "failed to parse CAM csv line %d for channel %d\n", c[ch].n_read+1, ch);
		perror("error:");
		return -1;
	}

	// read in png images from disk
	char img_path1[512];
	char img_path2[512];
	Mat img1, img2;
	if(en_debug){
		printf("reading in %dx%d image of format %d\n", d.width, d.height, d.format);
	}

	// read in the image from disk differently based on type
	if(d.format == IMAGE_FORMAT_RAW8){
		_construct_cam_path(ch, index, img_path1);
		img1 = imread(img_path1, IMREAD_GRAYSCALE);
		if(img1.type()!=CV_8UC1){
			fprintf(stderr, "ERROR loading %s\n", img_path1);
			fprintf(stderr, "expected 8-bit raw format\n");
			return -1;
		}
		d.stride=d.width;
		d.size_bytes=d.width*d.height;
	}
	else if(d.format == IMAGE_FORMAT_STEREO_RAW8){
		_construct_stereo_path(ch, index, img_path1,img_path2);
		img1 = imread(img_path1, IMREAD_GRAYSCALE);
		img2 = imread(img_path2, IMREAD_GRAYSCALE);
		if(img1.type()!=CV_8UC1){
			fprintf(stderr, "ERROR loading %s\n", img_path1);
			fprintf(stderr, "expected 8-bit raw format\n");
			return -1;
		}
		if(img2.type()!=CV_8UC1){
			fprintf(stderr, "ERROR loading %s\n", img_path2);
			fprintf(stderr, "expected 8-bit raw format\n");
			return -1;
		}
		d.stride=d.width;
		d.size_bytes=d.width*d.height*2;
	}
	else if(d.format == IMAGE_FORMAT_RAW16){
		_construct_cam_path(ch, index, img_path1);
		img1 = imread(img_path1, IMREAD_GRAYSCALE);
		if(img1.type()!=CV_16SC1){
			fprintf(stderr, "ERROR loading %s\n", img_path1);
			fprintf(stderr, "expected 16-bit signed format\n");
			return -1;
		}
		d.stride=d.width*2;
		d.size_bytes=d.width*d.height*2;
	}
	else{
		fprintf(stderr, "ERROR only support RAW_8, STEREO_RAW8, and RAW_16 images right now\n");
		fprintf(stderr, "got %d instead\n", d.format);
		return -1;
	}

	if(img1.size().width!=d.width || img1.size().height!=d.height){
		fprintf(stderr, "ERROR reading %s\n", img_path1);
		fprintf(stderr, "expected %dx%d, got %dx%d\n", d.width, d.height, img1.size().width, img1.size().height);
		return -1;
	}

	// correct the timestamp to line up with current time and wait if necessary
	// camera timestamp is at the beginning of the exposure, so add the exposure
	// time since we couldn't publish the camera frame before that in reality.
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns+d.exposure_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus a CAM frame with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}

	// send metadata first
	pipe_server_send_to_channel(ch, (char*)&d, sizeof(d));

	// for stereo send both images
	if(d.format == IMAGE_FORMAT_STEREO_RAW8){
		if(en_debug){
			printf("writing stereo frame, %d bytes\n", d.width*d.height);
		}
		pipe_server_send_to_channel(ch, (char*)img1.data, d.width*d.height);
		pipe_server_send_to_channel(ch, (char*)img2.data, d.width*d.height);
	}
	// otherwise send just the one
	else{
		if(en_debug){
			printf("writing mono frame, %d bytes\n", d.size_bytes);
		}
		pipe_server_send_to_channel(ch, (char*)img1.data, d.size_bytes);
	}

	return 0;
}


// todo, bundle up multiple imu samples since they are so fast.
static int _publish_vio(int ch, char* line)
{
	vio_data_t d;
	int ret;
	int index;
	int64_t timestamp_log;
	float rpy[3];
	float rpy_cam[3];
	int n_features, state;

	// scan data out of the line
	ret = sscanf(line, "%d,%ld,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%f,%d,%d\n",\
	&index, &timestamp_log,\
	&d.T_imu_wrt_vio[0], &d.T_imu_wrt_vio[1], &d.T_imu_wrt_vio[2],\
	&rpy[0],&rpy[1],&rpy[2],\
	&d.vel_imu_wrt_vio[0], &d.vel_imu_wrt_vio[1], &d.vel_imu_wrt_vio[2],\
	&d.imu_angular_vel[0], &d.imu_angular_vel[1], &d.imu_angular_vel[2],\
	&d.gravity_vector[0], &d.gravity_vector[1], &d.gravity_vector[2],\
	&d.T_cam_wrt_imu[0], &d.T_cam_wrt_imu[1], &d.T_cam_wrt_imu[2],\
	&rpy_cam[0],&rpy_cam[1],&rpy_cam[2],\
	&n_features,&d.quality, &state, &d.error_code);

	// make sure all fields were populated
	if(ret!=27){
		fprintf(stderr, "failed to parse VIO csv line %d for channel %d, read %d values\n", c[ch].n_read+1, ch, ret);
		perror("error:");
		return -1;
	}

	// do the rotation calculations
	_tait_bryan_xyz_to_rotation_matrix(rpy, d.R_imu_to_vio);
	_tait_bryan_xyz_to_rotation_matrix(rpy_cam, d.R_cam_to_imu);

	// fill in the rest. features are state are not ints, so they are cast here
	d.magic_number = VIO_MAGIC_NUMBER;
	d.n_feature_points = n_features;
	d.state = state;

	// correct the timestamp to line up with current time and wait if necessary
	d.timestamp_ns = _correct_timestamp_ns(timestamp_log);
	_wait_before_publish(ch, d.timestamp_ns);

	// write to pipe!
	if(en_debug){
		printf("publishing to ch%d at %ldus a VIO packet with timestamp %ldus\n", ch, _time_monotonic_ns()/1000,d.timestamp_ns/1000);
	}
	pipe_server_send_to_channel(ch, (char*)&d, sizeof(d));

	return 0;
}


static void* _publisher(void* arg)
{
	//int ret;
	int ch = (long)arg;
	char* line = NULL;
	size_t buflen = 1024;
	ssize_t line_len;

	if(c[ch].type == TYPE_IMU){
		printf("starting to publish imu to: %s\n", c[ch].out_pipe_path);
	}
	else if(c[ch].type == TYPE_CAM){
		printf("starting to publish cam to: %s\n", c[ch].out_pipe_path);
	}
	else if(c[ch].type == TYPE_VIO){
		printf("starting to publish vio to: %s\n", c[ch].out_pipe_path);
	}
	else{
		fprintf(stderr, "ERROR in publisher, unknown type %d\n", c[ch].type);
		return NULL;
	}

	// allocate a buffer for getline to read line into. getline will realloc
	// this buffer if its not big enough.
	line = (char*)malloc(buflen);
	if(line==NULL){
		perror("failed to allocate read buffer");
		c[ch].running = 0;
		return NULL;
	}

	// read in the csv header, this is a good test if the file is formed correctly
	line_len = getline(&line, &buflen, c[ch].csv_fd);
	if(line_len<10){
		perror("error reading csv header");
		c[ch].running = 0;
		return NULL;
	}
	if(en_debug){
		printf("header for channel %d %s\n", ch, c[ch].out_pipe_path);
		printf("%s", line);
	}
	c[ch].n_read++; // mark one line as read

	// keep running untill one of the many ending conditions is met:
	// main running is set to 0 by ctrl-c
	// c[ch].running is set to 0 by something
	// reach the end of the file
	// We don't look at the total samples recorded in the info.json file since
	// that may have not been updated correctly in even of a crash during logging
	// but the csv data may still be good and should be replayed anyway
	while(main_running && c[ch].running){
		// read the next line
		line_len = getline(&line, &buflen, c[ch].csv_fd);
		if(line_len<0){
			if(en_debug){
				printf("getline reached end of file for channel %d\n", ch);
			}
			c[ch].running = 0;
			return NULL;
		}
		c[ch].n_read++; // mark one line as read, not necessarily published yet

		// send line to appropriate publisher
		if(c[ch].type==TYPE_IMU)		_publish_imu(ch,line);
		else if(c[ch].type==TYPE_CAM)	_publish_cam(ch,line);
		else if(c[ch].type==TYPE_VIO)	_publish_vio(ch,line);

	}

	return NULL;
}





int main(int argc, char* argv[])
{
	int i;
	cJSON* parent;
	cJSON* channel_array;

	// start by parsing arguments
	if(_parse_opts(argc, argv)) return -1;

	// start signal handler so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	// try to open json info file
	char info_path[512];
	sprintf(info_path, "%sinfo.json", log_dir);
	if(en_debug) printf("opening json info file: %s\n", info_path);
	parent = json_read_file(info_path);
	if(parent==NULL){
		fprintf(stderr, "ERROR, couldn't find log at %s\n", log_dir);
		return -1;
	}

	// check format version
	int version;
	if(json_fetch_int(parent, "log_format_version", &version)){
		fprintf(stderr, "ERROR failed to find log_format_version in info.json\n");
		return -1;
	}
	if(en_debug) printf("using log_format_version=%d\n", version);

	// get number of channels in the log
	if(json_fetch_int(parent, "n_channels", &n_ch)){
		fprintf(stderr, "ERROR failed to find n_channels in info.json\n");
		return -1;
	}
	if(en_debug) printf("log contains %d channels\n", n_ch);

	// get start time of log
	// note cJSON uses 32-bit integers so we need to treat it as a double then
	// convert to 64-bit int.
	double log_start_time_double;
	if(json_fetch_double(parent, "start_time_monotonic_ns", &log_start_time_double)){
		fprintf(stderr, "ERROR failed to find start_time_monotonic_ns in info.json\n");
		return -1;
	}
	log_start_time_ns = log_start_time_double;
	if(en_debug) printf("log started at %ldns\n", log_start_time_ns);

	// grab the array of channels
	int array_len;
	channel_array = json_fetch_array(parent, "channels", &array_len);
	if(channel_array==NULL){
		fprintf(stderr, "ERROR failed to find channels array in info.json\n");
		return -1;
	}
	if(array_len!=n_ch){
		fprintf(stderr, "ERROR, log info.json claims to have %d channels but found %d\n", n_ch, array_len);
		return -1;
	}

	// load in data from each channel entry
	for(i=0;i<n_ch;i++){
		cJSON* array_item = cJSON_GetArrayItem(channel_array, i);

		if(array_item==NULL){
			fprintf(stderr, "ERROR, can't find channel %d json channel array\n", i);
		}
		if(json_fetch_int(array_item, "type", &c[i].type)){
			fprintf(stderr, "ERROR failed to find type for channel %d in info.json\n", i);
			return -1;
		}
		if(json_fetch_string(array_item, "pipe_path", c[i].out_pipe_path, MODAL_PIPE_MAX_PATH_LEN)){
			fprintf(stderr, "ERROR failed to find pipe_path for channel %d in info.json\n", i);
			return -1;
		}
		if(json_fetch_int(array_item, "n_samples", &c[i].n_total)){
			fprintf(stderr, "ERROR failed to find n_samples for channel %d in info.json\n", i);
			return -1;
		}
		// construct the path to the data for that channel, this will contain
		// the csv file and any other data/images for that channel
		sprintf(c[i].log_pipe_path,"%s%s", log_dir, c[i].out_pipe_path);

		// print this data out in debug mode
		if(en_debug){
			printf("log channel %d:\n", i);
			printf("    type: %s\n", _type_to_string(c[i].type));
			printf("    out path: %s\n", c[i].out_pipe_path);
			printf("    log path: %s\n", c[i].log_pipe_path);
			printf("    total samples: %d\n", c[i].n_total);
		}
	}

	// open all csv files
	for(i=0;i<n_ch;i++){
		char csv_path[512];
		sprintf(csv_path,"%sdata.csv", c[i].log_pipe_path);
		if(en_debug) printf("opening csv file: %s\n", csv_path);
		c[i].csv_fd = fopen(csv_path, "r");
		if(c[i].csv_fd==NULL){
			fprintf(stderr,"failed to open csv file: %s\n", csv_path);
			perror("error");
			return -1;
		}
	}

	// check all the server channels, most likely what we will find is that the
	// user already has voxl-camera-server or voxl-imu-server running and will
	// need to stop these first, so we check if the pipes exist already
	for(i=0;i<n_ch;i++){
		if(mkdir(c[i].out_pipe_path, S_IRWXU) && errno==EEXIST){
			fprintf(stderr, "pipe path already exists: %s\n", c[i].out_pipe_path);
			fprintf(stderr, "You need to make sure any conflicting servers are stopped first\n");
			fprintf(stderr, "for example:\n");
			fprintf(stderr, "systemctl stop voxl-imu-server && systemctl stop voxl-camera-server\n");
			return -1;
		}
	}

	// now open up server channels
	for(i=0;i<n_ch;i++){
		if(pipe_server_init_channel(i, c[i].out_pipe_path, 0)){
			fprintf(stderr, "failed to start server for %s\n", c[i].out_pipe_path);
			pipe_server_close_all();
			return -1;
		}
		if(c[i].type==TYPE_IMU){
			pipe_server_set_default_pipe_size(i, IMU_RECOMMENDED_PIPE_SIZE);
		}
		else if(c[i].type==TYPE_CAM){
			pipe_server_set_default_pipe_size(i, 8*1024*1024);
		}
		else if(c[i].type==TYPE_VIO){
			pipe_server_set_default_pipe_size(i, VIO_RECOMMENDED_PIPE_SIZE);
		}
		else{
			fprintf(stderr, "ERROR unknown log type: %d\n", c[i].type);
			pipe_server_close_all();
			return -1;
		}
	}

	// set main running flag to 1 so the threads about to be created don't just
	// exit right away
	main_running = 1;

	// record the current time, used for tweaking output timestamps
	replay_start_time_ns = _time_monotonic_ns();
	if(en_debug){
		printf("replay start time ns: %ld", replay_start_time_ns);
	}


	// start all the publisher threads
	for(i=0;i<n_ch;i++){
		c[i].running=1;
		pthread_create(&c[i].thread_id, NULL, _publisher, (void*)(long)i);
	}


	if(en_debug) printf("entering main loop\n");
	while(main_running){

		// check for the condition where all the threads stopped themselves
		int have_all_stopped = 1;
		for(i=0;i<n_ch;i++){
			if(c[i].running!=0){
				have_all_stopped = 0;
				break;
			}
		}
		if(have_all_stopped){
			if(en_debug) printf("all helpers stopped themselves\n");
			break;
		}

		// wait a bit
		usleep(100000);
	}


////////////////////////////////////////////////////////////////////////////////
// close everything
////////////////////////////////////////////////////////////////////////////////

	// close all the pipes
	if(en_debug) printf("closing pipes\n");
	pipe_server_close_all();

	printf("exiting cleanly\n");
	return 0;
}

