/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

#define CLIENT_NAME		"voxl-inspect-cam"
#define NUM_TIME_SAMPLES 10


static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int en_test = 0;
static int en_newline = 0;
static int pass = 0;

static unsigned long times[NUM_TIME_SAMPLES];
static int time_index = 0;
static int time_ready = 0;


static void _print_usage(void) {
	printf("\n\
\n\
Print out camera data from Modal Pipe Architecture.\n\
\n\
Options are:\n\
-h, --help       print this help message\n\
-n, --newline    print newline between each print instead of refreshing\n\
-t, --test       test mode with timeout, will print and exit after the\n\
                   the first successful frame, or abort after 2 seconds\n\
\n\
typical usage:\n\
/# voxl-inspect-cam tracking\n\
/# voxl-inspect-cam /run/mpa/tracking/\n\
\n\
For a simple pass/fail test:\n\
/# voxl-inspect-cam --test  tracking\n\
\n");
	return;
}



static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",               no_argument,        0, 'h'},
		{"newline",            no_argument,        0, 'n'},
		{"test",               no_argument,        0, 't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hnt", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 'n':
			en_newline = 1;
			break;
		case 't':
			en_test = 1;
			break;
		default:
			_print_usage();
			exit(0);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_client_construct_full_path(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// Calculate the average framerate from the last NUM_TIME_SAMPLES frames
static unsigned long calc_avg_fr_hz()
{
	double count = 0;
	unsigned long total = 0;
	//Check to see if we're in the middle of adding units to the array
	// i.e. index 0 is the successor to index length
	if(times[0] > times[NUM_TIME_SAMPLES - 1]){
		total = times[0] - times[NUM_TIME_SAMPLES - 1];
		count += 1.0;
	}

	for(int i = 0; i < NUM_TIME_SAMPLES - 1; i++){
		//Make sure this is not the border of where we're adding
		if(times[i] < times[i + 1]){
			count += 1.0;
			//calculate a rolling average to avoid overflow errors
			total *= (count - 1)/count;
			total += (times[i+1] - times[i]) / count;

		}
	}

	//Don't error check for sample count until we've filled in at least NUM_TIME_SAMPLES samples
	if(!time_ready && count == NUM_TIME_SAMPLES - 1) time_ready = 1;
	//Check to make sure we have the correct number of samples
	if(time_ready && count != NUM_TIME_SAMPLES - 1) fprintf(stderr, "\n\nError in calculating framerate :%f\n\n", count);


	return 1000000000/total;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL FONT_BOLD);
	printf("|size(bytes)");
	printf("| height ");
	printf("| width  ");
	printf("|exposure(ms)");
	printf("| gain ");
	printf("| frame id ");
	printf("|latency(ms)");
	printf("|Framerate(hz)");
	printf("| format");
	printf("\n");
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// camera helper callback whenever a frame arrives
static void _helper_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* context)
{

	printf("\r");
	printf("|%9d  ",	meta.size_bytes);
	printf("|%7d ",		meta.height);
	printf("|%7d ",		meta.width);
	printf("|     %6.1f ",	meta.exposure_ns/1000000.0);
	printf("|%5d ",		meta.gain);
	printf("|%8d  ",	meta.frame_id);
	printf("|    %6.1f ",	(_time_monotonic_ns()-(meta.timestamp_ns+meta.exposure_ns))/1000000.0);

	times[time_index++] = meta.timestamp_ns;
	time_index %= NUM_TIME_SAMPLES;

	printf("|    %5lu    ", calc_avg_fr_hz());
	printf("| %s", modal_image_format_name(meta.format));

	// cleanup the end of the line depending on mode
	if(en_newline)  printf("\n");
	fflush(stdout);
	return;
}


// camera frame callback registered to voxl-camera-server
static void _cam_test_cb(__attribute__((unused))int ch, camera_image_metadata_t meta, __attribute__((unused))char* frame, __attribute__((unused)) void* context)
{
	// make sure callback doesn't get called again
	main_running = 0;
	pipe_client_set_camera_helper_cb(0, NULL, NULL);

	// print some details
	printf("\n");
	printf("bytes:    %d\n",	meta.size_bytes);
	printf("width:    %d\n",	meta.width);
	printf("height:   %d\n",	meta.height);
	printf("exposure: %d\n",	meta.exposure_ns);
	printf("gain:     %d\n",	meta.gain);
	printf("frame_id: %d\n",	meta.frame_id);
	printf("format:   %s\n",	modal_image_format_name(meta.format));
	printf("\n");

	// check that width and height are reasonable
	if(meta.width<5 || meta.height<5){
		printf("FAIL: width/height too small: %dx%d\n", meta.width, meta.height);
		return;
	}

	// check that the size is reasonable
	if(meta.size_bytes<25){
		printf("FAIL: size_bytes too small: %d\n", meta.size_bytes);
		return;
	}

	// last check, make sure that there is at least one non-zero byte
	// indicate pass and return once found
	for(int i=0;i<meta.size_bytes;i++){
		if(frame[i]!=0){
			pass = 1;
			return;
		}
	}

	return;
}


static int _run_test(void)
{
	// request a new pipe from the server and assign callback
	// Don't use auto-reconnect mode!! we need to know if server is
	// running right away, init should fail if it's not.
	int ret = pipe_client_init_channel(0, pipe_path, CLIENT_NAME, \
										EN_PIPE_CLIENT_CAMERA_HELPER, \
										0);
	if(ret<0){
		pipe_client_print_error(ret);
		if(en_test) printf("FAIL\n\n");
		return -1;
	}

	pipe_client_set_camera_helper_cb(0, _cam_test_cb, NULL);
	clock_t start = clock();

	while(main_running && clock()-start < 20000) usleep(10000);

	// callback would have set main_running to 0 if we got a frame
	if(main_running){
		pass = 0;
		printf("TIMEOUT WAITING FOR CAMERA FRAME\n");
	}

	// print pass/fail and close up
	if(pass){
		printf("PASS\n\n");
		ret = 0;
	}
	else{
		printf("FAIL\n\n");
		ret = -1;
	}

	pipe_client_close_all();
	return ret;
}



int main(int argc, char* argv[])
{
	// check for options
	if(_parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// test mode
	if(en_test){
		return _run_test();
	}

	// normal non-test mode
	// set up all our MPA callbacks
	pipe_client_set_camera_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_init_channel(0, pipe_path, CLIENT_NAME, \
				EN_PIPE_CLIENT_CAMERA_HELPER | EN_PIPE_CLIENT_AUTO_RECONNECT, 0);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_client_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
