/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h>


#include <modal_pipe_client.h>
#include <modal_start_stop.h>

static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static float timeout_s = 2.0;


static void _print_usage(void)
{
	printf("\n\
Tool to stop the owning process for a given pipe name\n\
and clean up any dangling pipes that may remain.\n\
\n\
This will first send SIGINT to simulate ctrl-C and wait\n\
for the specified timeout (default 2 seconds).\n\
If the process does not exit gracefully in the timeout, it\n\
will be sent SIGKILL to force the process to stop with blind\n\
and barbaric fury.\n\
\n\
If the process does not exit gracefully, it may leave the\n\
specified pipe dangling in the file system. If that were to\n\
occur, this program will cleanup the pipe from the file system.\n\
\n\
You can perform this same operation with this function that is\n\
part of libmodal_pipe: kill_pipe_server_process(pipe_path, 2.0)\n\
\n\
-h, --help                print this help message\n\
-t, --timeout_s{seconds}  timeout to wait before sending SIGKILL\n\
                            default is 2.0 seconds.\n\
\n\
example useage:\n\
\n\
long method to kill voxl-imu-server:\n\
/# voxl_kill_pipe /run/mpa/imu0/\n\
\n\
shortcut method to kill voxl-camera-server\n\
/# voxl_kill_pipe tracking\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"timeout_s",		required_argument,	0,	't'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "ht:", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 't':
			timeout_s = atof(optarg);
			if(timeout_s<0.1f){
				fprintf(stderr, "ERROR: timeout must be >=0.1s\n");
			}
			exit(-1);
		default:
			_print_usage();
			exit(-1);
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_client_construct_full_path(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	kill_pipe_server_process(pipe_path, timeout_s);

	return 0;
}