/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <getopt.h>
#include <time.h>
#include <unistd.h>	// for usleep()
#include <string.h>
#include <stdlib.h> // for atoi()
#include <math.h>

#include "common.h"

#define MAX_SERVICES				32
#define MAX_SERVICE_NAME_LENGTH		128

#define SERVICE_TYPE_IDENTIFIER		".service"
#define SERVICE_TRUNCATION_LENGTH	strlen(SERVICE_TYPE_IDENTIFIER);

#define RUNNING					(1<<0)
#define ENABLED					(1<<1)
#define SET_RUNNING(i)			(i |= RUNNING)
#define SET_ENABLED(i)			(i |= ENABLED)
#define IS_RUNNING(i)			((i & RUNNING) > 0)
#define IS_ENABLED(i)			((i & ENABLED) > 0)

static int en_watch = 0;
static char dashes[MAX_SERVICE_NAME_LENGTH];

static void _print_usage(void)
{
	printf("\n\
typical usage\n\
/# voxl-inspect-services\n\
\n\
This will print out service status for all voxl, modal, or docker services.\n\
\n\
Options are:\n\
-h, --help                 print this help message\n\
-w, --watch                contnue checking every half second\n"
);
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",               no_argument,        0, 'h'},
		{"watch",              optional_argument,  0, 'w'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hw", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
			break;

		case 'h':
			_print_usage();
			return -1;

		case 'w':
			en_watch = 1;
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}

static int _get_status(char service[MAX_SERVICE_NAME_LENGTH])
{

	int status = 0;
	FILE *fp;
	char command[256];

	sprintf(command, "systemctl status %s | grep 'Active:*'", service);

	fp = popen(command, "r");
	if (fp == NULL) {
		return -1;
	}

	char line[1024];
	fgets(line, 1024, fp);
	pclose(fp);

	if(strstr(line, "running")) SET_RUNNING(status);

	sprintf(command, "systemctl status %s | grep 'Loaded:*'", service);

	fp = popen(command, "r");
	if (fp == NULL) {
		return -1;
	}

	fgets(line, 1024, fp);
	pclose(fp);

	if(strstr(line, "; enabled")) SET_ENABLED(status);

	return status;

}


static void _print_services(char services[MAX_SERVICES][MAX_SERVICE_NAME_LENGTH], int numServices, int maxLength)
{

	int status[numServices];
	char *running[2] = {COLOR_RED"Not Running"RESET_FONT,
						COLOR_GRN"  Running  "RESET_FONT};
	char *enabled[2] = {COLOR_RED"Disabled"RESET_FONT,
						COLOR_GRN" Enabled"RESET_FONT};

	for(int i = 0; i < numServices; i++){
		status[i] = _get_status(services[i]);
	}
	if(en_watch) printf(CLEAR_TERMINAL);
	printf(FONT_BOLD);
	printf(" %-*s |  Enabled  |   Running\n", maxLength, "Service Name");
	printf("%s----------------------------\n", dashes);
	printf(RESET_FONT);

	for(int i = 0; i < numServices; i++) {
		printf(" %-*s |", maxLength, services[i]);
		printf(" %s  |", enabled[IS_ENABLED(status[i])]);
		printf(" %s\n", running[IS_RUNNING(status[i])]);

	}

}


static void _make_dash_string(int length)
{

	char * s = dashes;
	sprintf(s, "%0*d", length, 0);
	while (*s == '0') *s++ = '-';

}


int main(int argc, char* argv[])
{

	// check for options
	if(_parse_opts(argc, argv)) return -1;


	do{
		FILE *fp;

		fp = popen("ls /etc/systemd/system/ | egrep 'voxl|modal|docker'", "r");
		if (fp == NULL) {
			printf("Failed to search for services\n" );
			return -1;
		}

		char services[MAX_SERVICES][MAX_SERVICE_NAME_LENGTH];
		int numServices = 0;
		int maxLength = strlen("Service Name");
		while (fgets(services[numServices], MAX_SERVICE_NAME_LENGTH, fp) != NULL) {
			int i = strlen(services[numServices]) - SERVICE_TRUNCATION_LENGTH;
			services[numServices][i - 1] = 0;
			if(i > maxLength) maxLength = i;
			numServices++;
		}
		pclose(fp);
		_make_dash_string(maxLength);

		if(numServices == 0){
			printf("No services detected\n");
			return -1;
		}

		_print_services(services, numServices, maxLength);
		usleep(50000);
	} while(en_watch);

	return 0;
}


