/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <getopt.h>
#include <unistd.h>	// for usleep()
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <modal_start_stop.h>
#include <modal_pipe_client.h>

#include "common.h"

#define CLIENT_NAME			"voxl-inspect-points"


// TOF width and height, used for special TOF use case
#define TOF_W		224
#define TOF_H		172


static char pipe_path[MODAL_PIPE_MAX_PATH_LEN];
static int newline = 0;



static void _print_usage(void)
{
	printf("\n\
Tool to print point cloud data to the screen for inspection.\n\
This will print up to the first 10 points received with each\n\
message. For point clouds read from the TOF sensor, this will\n\
print the 10 points across the middle of the image.\n\
\n\
-h, --help              print this help message\n\
-n, --newline           print newline between each pose\n\
\n\
typical usage:\n\
/# voxl-inspect-points tof_pc\n\
\n");
	return;
}


static int parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"help",			no_argument,		0,	'h'},
		{"newline",			no_argument,		0,	'n'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "hn", long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;
		case 'h':
			_print_usage();
			exit(0);
		case 'n':
			newline = 1;
			break;
		default:
			_print_usage();
			return -1;
		}
	}

	// scan through the non-flagged arguments for the desired pipe
	for(int i=optind; i<argc; i++){
		if(pipe_path[0]!=0){
			fprintf(stderr, "ERROR: Please specify only one pipe\n");
			_print_usage();
			exit(-1);
		}
		if(pipe_client_construct_full_path(argv[i], pipe_path)<0){
			fprintf(stderr, "ERROR: Invalid pipe name: %s\n", argv[i]);
			exit(-1);
		}
	}

	// make sure a pipe was given
	if(pipe_path[0] == 0){
		fprintf(stderr, "ERROR: You must specify a pipe name\n");
		_print_usage();
		exit(-1);
	}

	return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	printf(CLEAR_TERMINAL DISABLE_WRAP FONT_BOLD);

	printf("timestamp(ms)| points |");
	for(int i=0;i<10;i++){
		printf("  point%d  (XYZ)  |", i);
	}
	printf("\n");
	printf(RESET_FONT);
	return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context)
{
	fprintf(stderr, "\r" CLEAR_LINE FONT_BLINK "server disconnected" RESET_FONT);
	return;
}


// point cloud helper callback whenever a point cloud arrives
static void _helper_cb(__attribute__((unused))int ch, point_cloud_metadata_t meta, float* data, __attribute__((unused)) void* context)
{
	// normally start printing from the beginning of the array
	int start = 0;

	// print beginning fo data line
	if(!newline) printf("\r");
	printf("%12ld |%7d |", meta.timestamp_ns/1000000, meta.n_points);

	// only print up to 10 points
	int n_to_print = meta.n_points;
	if(n_to_print>10) n_to_print=10;

	// special case for TOF
	if(meta.n_points == (TOF_W*TOF_H)){
		// set pointer to the middle row, halfway back, and 5 points left
		// this should mean the 10 points printed sweeps the image center
		start = (((TOF_W*TOF_H)/2)-(TOF_W/2)-(5));
	}

	// print the data to be printed
	for(int i=0;i<n_to_print;i++){
		printf("%5.1f %5.1f %5.1f|",(double)(data[((start+i)*3)+0]),\
									(double)(data[((start+i)*3)+1]),\
									(double)(data[((start+i)*3)+2]));
	}

	if(newline) printf("\n");
	fflush(stdout);

	return;
}


int main(int argc, char* argv[])
{
	// check for options
	if(parse_opts(argc, argv)) return -1;

	// set some basic signal handling for safe shutdown.
	// quitting without cleanup up the pipe can result in the pipe staying
	// open and overflowing, so always cleanup properly!!!
	enable_signal_handler();
	main_running = 1;

	// set up all our MPA callbacks
	pipe_client_set_point_cloud_helper_cb(0, _helper_cb, NULL);
	pipe_client_set_connect_cb(0, _connect_cb, NULL);
	pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

	// request a new pipe from the server
	printf(CLEAR_TERMINAL "waiting for server at %s\n", pipe_path);
	int ret = pipe_client_init_channel(0, pipe_path, CLIENT_NAME, \
			EN_PIPE_CLIENT_POINT_CLOUD_HELPER| EN_PIPE_CLIENT_AUTO_RECONNECT, 1024);

	// check for errors trying to connect to the server pipe
	if(ret<0){
		pipe_client_print_error(ret);
		printf(ENABLE_WRAP);
		return -1;
	}

	// keep going until the  signal handler sets the running flag to 0
	while(main_running) usleep(500000);

	// all done, signal pipe read threads to stop
	printf("\nclosing and exiting\n" RESET_FONT ENABLE_WRAP);
	pipe_client_close_all();

	return 0;
}
