#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

USER=$(whoami)


print_usage () {
	echo "General Usage:"
	echo "voxl-configure-mpa"
	echo ""
	echo "To perform factory configuration for M500 with Flight Deck"
	echo "voxl-configure-mpa -f"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-mpa -h"
	echo ""
	exit 0
}



## print help message if requested
if [ "$1" == "-h" ]; then
	print_usage
	exit 0
elif [ "$1" == "--help" ]; then
	print_usage
	exit 0
fi

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi


## set factory -f flag if in factory mode
ARGS=""
if [ "$1" == "-f" ]; then
	ARGS="-f"
	echo "Starting Factory MPA setup"
fi


## now go through all configuration files
echo ""
echo "Configuring Extrinsics"
bash /usr/bin/voxl-configure-extrinsics ${ARGS}

echo ""
echo "Configuring Cameras"
bash /usr/bin/voxl-configure-cameras ${ARGS}

echo ""
echo "would you like to enable voxl-camera-server?"
echo "This is necessary for VIO and DFS"
select opt in "enable" "disable"; do
case $opt in
	enable )
		systemctl enable voxl-camera-server
		systemctl start voxl-camera-server
		break;;

	disable )
		systemctl disable voxl-camera-server
		systemctl stop voxl-camera-server
		break;;
	*)
		echo "invalid option"
		esac
done

echo ""
echo "Configuring QVIO"
bash /usr/bin/voxl-configure-qvio ${ARGS}

# echo ""
# echo "Configuring DFS"
# bash /usr/bin/voxl-configure-dfs ${ARGS}

# echo ""
# echo "Configuring voxl-streamer"
# bash /usr/bin/voxl-configure-streamer ${ARGS}

echo ""
echo "Configuring VOXL Vision PX4"
bash /usr/bin/voxl-configure-vision-px4 ${ARGS}


echo ""
echo "Done enabling services, reboot to complete setup"
echo "would you like to reboot now or later?"
select opt in "reboot_now" "reboot_later"; do
case $opt in
	reboot_now )
		echo ""
		echo "rebooting now"
		reboot
		break;;

	reboot_later )
		break;;
	*)
		echo "invalid option"
		esac
done

echo ""
echo "Finished running voxl-configure-mpa"

exit 0
