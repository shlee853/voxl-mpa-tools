#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CONFIG_FILE=/etc/modalai/extrinsics.conf
USER=$(whoami)


print_usage () {
	echo "General Usage:"
	echo "voxl-configure-extrinsics"
	echo ""
	echo "To perform factory configuration for VOXL Flight Deck"
	echo "voxl-configure-extrinsics -f"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-extrinsics -h"
	echo ""
	exit 0
}


## print help message if requested
if [ "$1" == "-h" ]; then
	print_usage
	exit 0
elif [ "$1" == "--help" ]; then
	print_usage
	exit 0
fi

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi

## wipe file and repopulate if we are doing a factory setup
if [ "$1" == "-f" ]; then
	echo "setting extrinsic config file to factory defaults"
	echo "extrinsic config file: ${CONFIG_FILE}"
	cp -f /usr/share/modalai/extrinsic_configs/M500_extrinsics.conf ${CONFIG_FILE}

## otherwise ask user what they want to do
else
	echo "configuring ${CONFIG_FILE}"
	echo "Would you like to leave this extrinsics config file alone,"
	echo "reset to factory defaults, or use a preset config?"
	select opt in "leave_alone" "defaults" "preset"; do
	case $opt in
		leave_alone )
			echo ""
			echo "leaving file alone and exiting"
			exit 0
			break;;

		defaults )
			echo ""
			echo "resetting extrinsics config file to factory defaults"
			echo "note, you can do this in the future with voxl-configure-extrinsics -f"
			cp -f /usr/share/modalai/extrinsic_configs/M500_extrinsics.conf ${CONFIG_FILE}
			exit 0
			break;;

		preset )
			echo ""
			echo "which preset would you like to use?"
			echo "Note: M500 is same as factory default"
			echo "more presets coming soon!!"
			select opt in "ModalAI_M500" "M500_VOXLCAM" "ModalAI_Mapper_v1"; do
			case $opt in
				ModalAI_M500 )
					cp -f /usr/share/modalai/extrinsic_configs/M500_extrinsics.conf ${CONFIG_FILE}
					break;;
				M500_VOXLCAM )
					cp -f /usr/share/modalai/extrinsic_configs/M500_VOXLCAM_extrinsics.conf ${CONFIG_FILE}
					break;;
				ModalAI_Mapper_v1 )
					cp -f /usr/share/modalai/extrinsic_configs/mapper_v1_extrinsics.conf ${CONFIG_FILE}
					break;;
				*)
					echo "invalid option"
					esac
			done
			break;; # end of preset answer
		*)
			echo "invalid option"
			esac
	done
fi


# re-parse file to add anything the presets may have missed
echo "loading and updating config file with voxl-inspect-extrinsics"
voxl-inspect-extrinsics -q

echo "Done configuring extrinsics"
